var express 	           = require('express'),
	config 		= require("./configs"),
	api 		= require('./api'),
	users 		= require('./account'),
	viewer 		= require('./viewer'),
            search             = require('./Search'),
            officials            = require('./brgyofficial'),
            business            = require('./business'),
	app 		= express();

app

.use(express.static('./public'))
.use(users)
.use('/api', api)
.use('/search', search)
.use('/viewer', viewer)
.use('/users', users)
.use('/brgyofficials', officials)
.use('/businesspermits', business)
.get('*', function (req, res){

	res.sendfile('public/index.html');

})

.listen(config.server.port);
 console.log(config.message);
 console.log('Database URL running "' + config.mongodb.brgydata + '..."');
 console.log("Server listening on port " + config.server.port);
