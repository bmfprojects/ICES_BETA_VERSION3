angular.module('brgyapp')

.factory('Contact', function($http, $resource){
    return $resource('/api/clearance/:id', {},{
    	'update' : { method: 'PUT', isArray: true }
    });
})

.factory('Resident', function($resource) {
  return $resource('/api/resident/:id', { id: '@_id' }, {
    update: {
      method: 'PUT' // this method issues a PUT request
    }
  });
});