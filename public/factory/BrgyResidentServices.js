'use strict';

var app = angular.module('ResidentService', []);

app.factory("ResidentServices",  function($http, DbCollection, SearchBaseurl){

return{

    GetSpecificGender:function(gender){
        return $http.get(DbCollection+'getgender/'+gender);
     },
    GetSpecificFullname:function(fullname){
        return $http.get(SearchBaseurl+'getfullname/'+fullname);
     },

    GetSpecificBloodType:function(bloodtype){
        return $http.get(SearchBaseurl+'getbloodtype/'+bloodtype);
    },

    GetSpecifichealthAvailed:function(health){
        return $http.get(SearchBaseurl+'healthavailed/'+health);
    },
    GetSpecificOccupation:function(occupation){
        return $http.get(SearchBaseurl+'getoccupation/'+occupation);
    },
    UpdateResident:function(id,data){
        return $http.put(DbCollection+id, data);
    },
    GetResident:function(id){
        return $http.get(DbCollection+'/resident/'+id);
    },
    GetResidentofficials:function(id){
        return $http.get(DbCollection+'/residentofficials/'+id);
    },
    GetResidentbirthday:function(){
        return $http.get(DbCollection+'/resident');
    },
    GetClearance:function(id){
        return $http.get(DbCollection + '/clearance/'+id);
    }

}

});