'use strict';

var app = angular.module('BusinessFactories', []);

app.factory("BusinessFactory",  function($http,  BusinessCollections){

return{

    CreateBusiness:function(data){
        return $http.post(BusinessCollections+'/business', data);
    },

    GetAllBusiness:function(){
        return $http.get(BusinessCollections+'/business');
     },

    GetBusinessID:function(id){
        return $http.get(BusinessCollections+'/business/'+id);
    },

    UpdateBusiness:function(id,data){
        return $http.put(BusinessCollections+'/business/'+id, data);
    },

    DeleteBusiness:function(id){

        return $http.delete(BusinessCollections+'/business/'+id);
    }

}

});