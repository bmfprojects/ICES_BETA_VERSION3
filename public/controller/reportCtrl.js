'use strict';

var app = angular.module('brgyapp');

app.controller('reportCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ReportService, ResidentServices, getreport){

$scope.tabmonth = ItemsServices.tabmonth();
$scope.selectYears = ItemsServices.searchYear();
$scope.Datevalue = ItemsServices.Datevalue();
$scope.searchtypes = ItemsServices.searchType();
$scope.searchtypes.searchtype = {value: 2015, searchYear: 2015};

    var month = 'January';
    var brgyID = $scope.UserAcount.barangay._id;

    $scope.dailyoption = false;
    $scope.dailyReport = function(){
    $scope.dailyoption = true;
    }

    $scope.sum = function(items, prop){
    return items.reduce( function(a, b){
        return a + b[prop];
        }, 0);
    };

    var column = [
            {title: "OR No.", key: "OR"},
            {title: "Given Price", key: "clearance_price"},
            {title: "Resident Name", key: "residentName"},
            {title: "Remark", key: "Remark"},
            {title: "Date of Issued", key: "dateofissue"}
   ];
 $scope.generatedata =  function(getmonth){

        var days,
            month;

        var d = new Date(getmonth);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd      = days[d.getUTCDay() +1];
        var mm  = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();

        var todays = mm+' '+dy+', '+ yyyy;
        return todays;
};

$scope.activedaily = false;
$scope.error = "";
$scope.getdaily = function(dailyrecord){
    $scope.error = "";
    if(dailyrecord){
    $scope.activedaily = true;
    $scope.dailydateshow = dailyrecord;

                var getmonth         = ItemsServices.getmonth(dailyrecord);
                var getday              = ItemsServices.getday(dailyrecord);
                var getdatevalue    = ItemsServices.getdatevalue(dailyrecord);
                var get_issuedYear = ItemsServices.get_issuedYear(dailyrecord);

                var getdetails = {
                    brgyID: brgyID,
                    month : getmonth,
                    datevalue : getdatevalue,
                    yearofdate : get_issuedYear,
                    dayofvalue : getday
                }

               $scope.getmonth = function(brgyID){

                 ReportService.GetDaily(getdetails)
                     .then(function(results){
                    $scope.salesreport = results.data;
                    $scope.Totalsales =  $scope.sum($scope.salesreport, 'clearance_price');
                    ReportService.GetDaily(getdetails)
                    .success(function(getdata){
                        var report_res = [];
                         for (var i = 0; i < getdata.length; i++) {
                             var results = getdata[i];

                                  report_res.push({
                                      OR :  results.OR,
                                      clearance_price: results.clearance_price,
                                      residentName: results.residentName,
                                      dateofissue: $scope.generatedata(results.jud_issuedON),
                                      Remark: results.remark,
                                    });

                                  $scope.download_report_daily = function(){
                                    var getmonthlyreport, todays, doc;
                                        ReportService.GetDaily(getdetails)
                                        .then(function(result){

                                            getmonthlyreport = result.data;

                                              var totalPagesExp = "{total_pages_count_string}";
                                                var doc = new jsPDF('p', 'pt');
                                                var header = function (doc, pageCount, options) {
                                                    doc.setFontSize(17);
                                                    doc.text("Result of "+getmonth+' '+getdatevalue+', '+get_issuedYear, options.margins.horizontal, 60);
                                                    doc.setFontSize(options.fontSize);
                                                };
                                                var footer = function (doc, lastCellPos, pageCount, options) {
                                                    var str = "Page " + pageCount + " of " + totalPagesExp + "                                                  TOTAL : Php "+ $scope.Totalsales ;
                                                    doc.text(str, options.margins.horizontal, doc.internal.pageSize.height - 30);
                                                };
                                                var options = {renderHeader: header, renderFooter: footer, margins: {horizontal: 40, top: 80, bottom: 50}};
                                                doc.autoTable(column, report_res, options);
                                                doc.putTotalPages(totalPagesExp);

                                                doc.addPage();

                                                doc.save(getmonth+' '+getdatevalue+', '+get_issuedYear+'.pdf');

                                        });
                                    }
                        }
                    })

                });
            }

            $scope.getmonth(getdetails);

    }else{
     $scope.error = 'please select the daily report.';
    }

}


    $scope.$watch('searchtypes.searchtype', function(YearSelected){
    $scope.activedaily = false;
        $scope.getmonth = function(brgyID, month){
           $scope.activedaily = false;
            var getyeardetails= {
                brgyIDs : brgyID,
                getmonth : month,
                yearvalue : YearSelected.searchYear
            };
            ReportService.getmonthreport_res(getyeardetails)
            .success(function(getdata){
                var report_res = [];
                 for (var i = 0; i < getdata.length; i++) {
                     var results = getdata[i];

                      report_res.push({
                          OR :  results.OR,
                          clearance_price: results.clearance_price,
                          residentName: results.residentName,
                          dateofissue: $scope.generatedata(results.jud_issuedON),
                          Remark: results.remark,
                        });

                        $scope.download_monthly_report = function(){
                        var getmonthlyreport, todays, doc;
                            ReportService.GetSpecificYear(getyeardetails)
                            .then(function(result){

                                getmonthlyreport = result.data;

                                  var totalPagesExp = "{total_pages_count_string}";
                                    var doc = new jsPDF('p', 'pt');
                                    var header = function (doc, pageCount, options) {
                                        doc.setFontSize(17);
                                        doc.text("Result of "+getyeardetails.getmonth+' '+getyeardetails.yearvalue, options.margins.horizontal, 60);
                                        doc.setFontSize(options.fontSize);
                                    };
                                    var footer = function (doc, lastCellPos, pageCount, options) {
                                        var str = "Page " + pageCount + " of " + totalPagesExp + "                                                  TOTAL : Php "+ $scope.Totalsales ;
                                        doc.text(str, options.margins.horizontal, doc.internal.pageSize.height - 30);
                                    };
                                    var options = {renderHeader: header, renderFooter: footer, margins: {horizontal: 40, top: 80, bottom: 50}};
                                    doc.autoTable(column, report_res, options);
                                    doc.putTotalPages(totalPagesExp);
                                    doc.save(getyeardetails.getmonth+', '+getyeardetails.yearvalue+'.pdf');

                            });
                        }

                }

            });

             ReportService.GetSpecificYear(getyeardetails)
                 .then(function(results){
                $scope.salesreport = results.data;

                $scope.Totalsales =  $scope.sum($scope.salesreport, 'clearance_price');

            });

        }

        $scope.getmonth(brgyID, month);

        $scope.clearSearch = function(){

            $scope.searchyear= "";
            $scope.dailyoption = false;
            $scope.searchtypes.searchtype = {value: 2015, searchYear: 2015};
            $scope.getmonth(brgyID, month);

        }

    });

});





















//=======================================
//backup source code important

//same as above function
// Array.prototype.sum = function (prop) {
//     var total = 0
//     for ( var i = 0, _len = this.length; i < _len; i++ ) {
//         total += this[i][prop]
//     }
//     return total
// }


// var totals = getreport.data.reduce(function(c,x){
//     if(!c[x._id])
//         c[x._id] = {
//         OR: x.OR,
//         _id: x.residentId,
//         total_sales: 0

//     };
//     c[x._id].total_sales += Number(x.clearance_price);
//     return c;
// }, {});

// console.log(totals);
// $scope.textsample = '';

// $scope.totalAmount = function(){
//        var total = 0;
//        for (var i = 0; i < getreport.data.length; i++) {
//               $scope.textsample +=  getreport.data[i].clearance_price;
//             }
//        return total;
// }