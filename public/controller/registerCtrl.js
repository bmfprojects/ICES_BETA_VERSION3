'use strict';

var app = angular.module('brgyapp');

app.controller('registerCtrl', function($scope, $window, $rootScope, ngTableParams,  $modal, $log, Restangular, $http, $q, $filter, $location, $timeout, DbCollection){
        
// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

     $http.get(DbCollection + 'resident/')
     .then(function(result){
       $scope.resident = result.data
       var people = $scope.resident.length
       $rootScope.people = people;
     });

 $http.get(DbCollection + 'provincelist/')
     .then(function(result){
       $scope.province_account = result.data

      $rootScope.account_count = $scope.province_account.length
       return $scope.provinceparams = new ngTableParams(
       {
          page: 1,            // show first page
          count: 5           // count per page
      }, 
      {
          total: $scope.province_account.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.province_account, params.orderBy()) :
                      $scope.province_account;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      }
      );
 });

 $http.get(DbCollection + 'brgy_userlist/')
     .then(function(result){
       $scope.barangayaccount = result.data

      $rootScope.account_count = $scope.barangayaccount.length
       return $scope.barangayaccountparams = new ngTableParams(
       {
          page: 1,            // show first page
          count: 5           // count per page
      }, 
      {
          total: $scope.barangayaccount.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.barangayaccount, params.orderBy()) :
                      $scope.barangayaccount;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      }
      );
 });

 $http.get(DbCollection + 'registered/')
     .then(function(result){
       $scope.useraccount = result.data

      $rootScope.account_count = $scope.useraccount.length
       return $scope.municipalityparams = new ngTableParams(
       {
          page: 1,            // show first page
          count: 5           // count per page
      }, 
      {
          total: $scope.useraccount.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.useraccount, params.orderBy()) :
                      $scope.useraccount;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      }
      );
 });


      $scope.editbarangay = function (size, id) {


               if($rootScope.UserAcount.accesscontrol === 'provincecontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/provinces.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/brgy_userlist/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'regionadmin'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/brgy_userlist/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/brgy_userlist/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }
       

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

          };

       $scope.editmunicipal = function (size, id) {


               if($rootScope.UserAcount.accesscontrol === 'provincecontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/provinces.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/registered/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'regionadmin'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/registered/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.model,
                    size: size,
                    resolve: {
                      getuser: function($http){
                          if(id){
                            return $http.get(DbCollection + '/registered/'+ id);
                          }else{
                            return null;
                            
                          }
                        }
                      }

                    });

               }
       

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

          };

        $scope.Editprovince = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/register.html',
            controller: $scope.model,
            size: size,
            resolve: {
                  getuser: function($http){
                      if(id){
                        return $http.get(DbCollection + '/provincelist/'+ id);
                      }else{
                        return null;
                        
                      }
                    }
                  }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

          };

        $scope.addusers = function (size) {
       

               if($rootScope.UserAcount.accesscontrol === 'provincecontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/provinces.html',
                    controller: $scope.adduser,
                    size: size

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'regionadmin'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.adduser,
                    size: size

                    });

               }else if($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

                    var modalInstance = $modal.open({

                    templateUrl: '../views/register.html',
                    controller: $scope.adduser,
                    size: size

                    });

               }



              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());
                   // window.location.reload();
                });

          };

    $scope.deactivate = function(id, active, access){

          if(active == 'Active'){

            $scope.account = {
              active : 'Deactivated',
              active_caption: 'Activate',
              access : access

            };

              $http.put(DbCollection+'/account_deactivate/'+id, $scope.account)
              .then(function(result){
                 window.location.reload();
              }, function (error) {
                $scope.error = error.data.message;
              });

          }else if(active == 'Deactivated'){

            $scope.account = {
              active : 'Active',
              active_caption: 'Deactivate',
              access : access

            };

              $http.put(DbCollection+'/account_deactivate/'+id, $scope.account)
              .then(function(result){
                window.location.reload();
              }, function (error) {
                $scope.error = error.data.message;
              });
          }

    };


      $scope.adduser = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
      
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.caption_pass = 'Change Password';

          $scope.enable_new = function (){
            $scope.newpass = 'true';
            $scope.caption_pass = 'Click to hide';
          }
          $scope.enable_hide = function (){
            $scope.newpass = 'false';
            $scope.caption_pass = 'Change Password';
          }
        

if ($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('UserAcount.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('UserAcount.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('UserAcount.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });

}else if($rootScope.UserAcount.accesscontrol === 'regionadmin'){

      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('account.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('account.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('account.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });
}else if($rootScope.UserAcount.accesscontrol === 'provincecontrol'){
      
      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('UserAcount.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('UserAcount.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('account.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });
}


      $scope.myaccount = 'Create Account';

          $scope.take_snapshot = function(data_uri) {
                  // take snapshot and get image data
                  Webcam.snap( function(data_uri) {
                      // display results in page

                      $scope.account.imageuri = data_uri;
                      console.log($scope.account.imageuri);
                      document.getElementById('capture_image').innerHTML = 
                          // '<h2>Here is your image:</h2>' + 
                          '<img id="myImg" src="'+data_uri+'"/>' +
                          '<input type="hidden" id="resulturi" ng-model="account.imageuri" value="'+ $scope.account.imageuri +'" >';

                  });

                      var x = document.getElementById("myImg").src;
                      var x_result = x.toString();
                        
          }


         $http.get(DbCollection + 'account/')
            .then(function(result){
            $rootScope.UserAcount = result.data;
            });


        $scope.save = function () {
          console.log($scope.account)
        $http.post(DbCollection + '/registered', $scope.account)
        .then(function (result) {
                $scope.account = result.data;
                $modalInstance.dismiss('cancel');
                window.location.reload();
        }, function (error) {
                $scope.error = error.data.message;
                console.log($scope.account);
            });
        };


      }

      $scope.model = function($scope, $modalInstance, $modal, getuser, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
      
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.caption_pass = 'Change Password';

          $scope.enable_new = function (){
            $scope.newpass = 'true';
            $scope.caption_pass = 'Click to hide';
          }
          $scope.enable_hide = function (){
            $scope.newpass = 'false';
            $scope.caption_pass = 'Change Password';
          }
        

if ($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('UserAcount.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('UserAcount.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('UserAcount.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });

}else if($rootScope.UserAcount.accesscontrol === 'regionadmin'){

      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('account.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('account.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('account.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });
}else if($rootScope.UserAcount.accesscontrol === 'provincecontrol'){
      
      $http.get(DbCollection+'/region')
      .then(function(result){
        $scope.r = result.data;
      });

      $scope.$watch('UserAcount.region._id', function(id){
          $http.get(DbCollection+'/province/'+id)
          .then(function(result){
           $scope.p = result.data;
          })
     });

      $scope.$watch('UserAcount.province._id', function(id){
          $http.get(DbCollection+'/municipality/'+id)
          .then(function(result){
           $scope.m = result.data;
          })
      });

      $scope.$watch('account.municipality._id', function(id){
          $http.get(DbCollection+'/brgycol/'+id)
          .then(function(result){
           $scope.b = result.data;
          })
      });
}


      $scope.myaccount = 'Create Account';

          $scope.take_snapshot = function(data_uri) {
                  // take snapshot and get image data
                  Webcam.snap( function(data_uri) {
                      // display results in page

                      $scope.account.imageuri = data_uri;
                      console.log($scope.account.imageuri);
                      document.getElementById('capture_image').innerHTML = 
                          // '<h2>Here is your image:</h2>' + 
                          '<img id="myImg" src="'+data_uri+'"/>' +
                          '<input type="hidden" id="resulturi" ng-model="account.imageuri" value="'+ $scope.account.imageuri +'" >';

                  });

                      var x = document.getElementById("myImg").src;
                      var x_result = x.toString();
                        
          }


        $scope.update = function (id, access) {
        
         if(access === 'user'){
            console.log(access);
              $http.put(DbCollection + '/edit_own_account/'+id, $scope.account)
                .then(function (result) {
                      $scope.account = result.data;
                      $modalInstance.dismiss('cancel');
                     
                }, function (error) {
                        $scope.error = error.data.message;
                });

           }else if(access === 'barangayaccesscontrol'){
              console.log(access);
               $http.put(DbCollection + '/brgy_userlist/'+id, $scope.account)
                .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                      
                }, function (error) {
                        $scope.error = error.data.message;
                });
           }else if(access === 'municipalitycontrol'){
              console.log(access);
               $http.put(DbCollection + '/registered/'+id, $scope.account)
                .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                      
                }, function (error) {
                        $scope.error = error.data.message;
                });
           }else if(access === 'provincecontrol'){
              console.log(access);
               $http.put(DbCollection + '/provincelist/'+id, $scope.account)
                .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                      
                }, function (error) {
                        $scope.error = error.data.message;
                });
           }

        };

console.log(getuser.data);
        if(getuser){
          return $scope.account = getuser.data;
        }else{
          return null;
        }

    }

});



app.controller('editaccountCtrl', function($scope, $rootScope, Account, $http, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
	      

 $scope.deactivate = function(id){
 	console.log(id);
	 $http.put(DbCollection + 'registered/'+ id, $scope.account)
     .then(function(result){
      $scope.result2 = result.data;
    	$location.path('/user');
     }); 
 }

  $scope.take_snapshot = function(data_uri) {
          // take snapshot and get image data
          Webcam.snap( function(data_uri) {
              // display results in page

              $scope.account.imageuri = data_uri;
              console.log($scope.account.imageuri);
              document.getElementById('capture_image').innerHTML = 
                  // '<h2>Here is your image:</h2>' + 
                  '<img id="myImg" src="'+data_uri+'"/>' +
                  '<input type="hidden" id="resulturi" ng-model="account.imageuri" value="'+ $scope.account.imageuri +'" >';

          });

              var x = document.getElementById("myImg").src;
              var x_result = x.toString();
                
  }

	 $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
      });

      var currentmunicipal = $scope.UserAcount.municipality;


         $rootScope.myaccount = 'My Account'

		 $http.get(DbCollection + 'account/')
	      .then(function(result){
	      return $rootScope.UserAcount = result.data;
	      console.log($scope.UserAcount);

	      });

	      $scope.accountid = Account;

		  $scope.account = Restangular.copy($scope.accountid);

		  $scope.isClean = function() {
		    return angular.equals($scope.account, $scope.account);
		  }

		  $scope.destroy = function() {
		    $scope.account.remove().then(function() {
		      $location.path('/user');
		    });
		  };

		  $scope.save = function() {
		    $scope.account.put().then(function() {
		      $location.path('/user');
		    });
		  };
	
});