'use strict';

var app = angular.module('brgyapp');

app.controller('LoginController', function($scope, $rootScope, $window, $timeout, $location, authenticationSvc, DbCollection) {
      
        $scope.userInfo = null;
            $scope.login = function () {
            authenticationSvc.login($scope.account)
                .then(function (result) {
                    $scope.dataLoading = true;
                    $scope.userInfo = result.data;
                    $rootScope.UserAcount = result.data;
                    $location.path("/home");
                    window.location.reload();
                }, function (error) {
                    $scope.error = error.data.message;
                    $scope.dataLoading = false;
                    $location.path("/login");
                });
            };

 });

app.controller('logoutCtrl', function($scope, $http, loginCollection, $rootScope, $window, $location, authenticationSvc, DbCollection) {
    $scope.logout = function () {
        $http.post(loginCollection +'/logout');
            console.log(loginCollection);
            delete $scope.userInfo;
            delete $window.sessionStorage["userInfo"];
            $scope.result = 'You have been logging out.'
            $location.url('/login');
            window.location.reload();

    }
 });