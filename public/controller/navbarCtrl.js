'use strict';

var app = angular.module('brgyapp');

app.controller('navbarCtrl', function($rootScope, ResidentServices, ReportService, BrgyOfficialcollections, ItemsServices, BrgyOfficial, $modal, DbCollection, $http, $scope, Restangular){


      var date_today = new Date();
      var dbmonth = ItemsServices.getmonth(date_today);
      var bvalue    = ItemsServices.getdatevalue(date_today);
      var byear = ItemsServices.get_issuedYear(date_today);

        var passtobirth = {
          dvalue : bvalue,
          dmonth : dbmonth,
          dyear : byear
        };

        ReportService.GetresidentBirthday(passtobirth)
        .then(function(dataresult){

            if(dataresult){
              $scope.residentBirthdays = dataresult.data;
            }else{
              $scope.nobirthday = 'No birthdays found.';
            }

        });

        $scope.get_age = function(d){
             return ItemsServices.calculate_age(d);
        }

       $scope.viewbirthday = function(size, id){

            var modalInstance = $modal.open({
              templateUrl: '../views/residentBirthdays.html',
              controller: $scope.residentbirthdayCtrl,
              size: size,
              resolve: {
                     getbirthwishes: function(){
                          return $http.get(DbCollection+'/resident/'+ id)
                    }
              }

            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {

              });
       }

      $scope.residentbirthdayCtrl = function($scope, getbirthwishes, $modalInstance){

         $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        $scope.get_age = function(d){
             return ItemsServices.calculate_age(d);
        }

        $scope.residentbirth = getbirthwishes.data;
      }

       $scope.login_modal = function (size) {

            var modalInstance = $modal.open({
              templateUrl: '../views/login.html',
              controller: $scope.loginCtrl,
              size: size,

            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };
 $scope.loginCtrl = function($scope, $rootScope, $modalInstance, $window,  $timeout, $location, authenticationSvc, DbCollection) {

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };
         $scope.userInfo = null;
            $scope.login = function () {
            authenticationSvc.login($scope.account)
                .then(function (result) {
                    $scope.dataLoading = true;
                    $scope.userInfo = result.data;
                    $rootScope.UserAcount = result.data;
                    $location.path("/home");
                    window.location.reload();
                }, function (error) {
                    $scope.error = error.data.message;
                    $scope.dataLoading = false;
                    $location.path("/login");
                });
            };


       $scope.signup = function (size) {

        var modalInstance = $modal.open({
          templateUrl: '../views/signup.html',
          controller: $scope.signupCtrl,
          size: size,

        });


        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
            // window.location.reload();
          });

      };

    $scope.signupCtrl = function($scope, $rootScope, $modalInstance, $http, $window, $timeout, $location, Signupviewers, authenticationSvc, DbCollection) {

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };


       $scope.signup_viewer = function () {
          console.log($scope.account)
        $http.post(Signupviewers + '/viewers', $scope.account)
        .then(function (result) {
                $scope.account = result.data[0];
                $modalInstance.dismiss('cancel');
                $rootScope.message_access = "You now have access with your new account '"+ $scope.account.username + "' you can now login as viewer.";

        }, function (error) {
                $scope.error = error.data.message;
                console.log($scope.account);
            });
        };
    }

 }
	  $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
        if(result.data){
            if($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

              var id = $rootScope.UserAcount.municipality._id;

              $http.get(DbCollection + 'brgycol/'+ id)
                .then(function(result){
                  $rootScope.count_brgy = result.data.length;
                });
          }
        }else{
         // nothing..
        }



      });


$scope.$watch('qrvalue', function(value){

if(value){
var l = value.length;
var string_l = l.toString();

if(string_l === '8'){

    $http.get(DbCollection + '/get_qr_resident/'+ value)
    .then(function (result) {
           $scope.viewqr('lg', value);
    }, function (error) {
          $scope.error = error.data.message;
    });

}else if( l < 8){

  $scope.error = 'Please enter the 8 characters!';

}else if(l > 8){

  $scope.error = 'The data length is exceed!'

}


}
       $scope.viewqr = function (size, value) {

            var modalInstance = $modal.open({
              templateUrl: '../views/viewresident.html',
              controller: $scope.viewresident,
              size: size,
               resolve: {
                      getvalue: function($http){
                          if(value){
                            return $http.get(DbCollection + '/get_qr_resident/'+ value);
                          }else{
                            return null;

                          }
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };

    $scope.viewresident = function($scope, $modalInstance, getvalue, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

     $scope.brgyresident = getvalue.data[0];
      console.log($scope.brgyresident);
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            window.location.reload();
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            window.location.reload();
          };

    }

})


     $scope.account_modal = function (size, id, access) {

          var modalInstance = $modal.open({
            templateUrl: '../views/my_account.html',
            controller: $scope.editmodal,
            size: size,
             resolve: {
                    getaccount: function($http){


                          if(access === 'regionadmin'){

                              return $http.get(DbCollection + '/registered/'+ id);

                          }else if(access === 'provincecontrol'){

                              return $http.get(DbCollection + '/provincelist/'+ id);

                          }else if(access === 'municipalitycontrol'){

                              return $http.get(DbCollection + '/registered/'+ id);

                          }else if(access === 'barangayaccesscontrol'){

                              return $http.get(DbCollection + '/brgy_userlist/'+ id);

                          }else{

                            return null;

                          };

                      },
                      auth: function ($q, authenticationSvc) {
                          var userInfo = authenticationSvc.getUserInfo();
                          if (userInfo) {
                              return $q.when(userInfo);
                          } else {
                              return $q.reject({ authenticated: false });
                          }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              window.location.reload();
            });

    };

     $scope.editmodal = function($scope, $modalInstance, getaccount, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {



      if(getaccount.data === null){
        console.log('no data found!')
      }else{
        $scope.account = getaccount.data;
      }

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

     	   $scope.caption_pass = 'Change Password';

          $scope.enable_new = function (){
            $scope.newpass = 'true';
            $scope.caption_pass = 'Click to hide';
          }
          $scope.enable_hide = function (){
            $scope.newpass = 'false';
            $scope.caption_pass = 'Change Password';
          }

          $scope.update_my_account = function (id, access) {

             if(access === 'user'){

                $http.put(DbCollection + '/edit_own_account/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else if(access === 'provincecontrol'){

                  $http.put(DbCollection + '/provincelist/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else if(access === 'municipalitycontrol'){

                  $http.put(DbCollection + '/municipalityaccount/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else{

                $http.put(DbCollection + '/registered/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });
             }

  	   };

     };

$scope.officials_modal = function(size){

        var modalInstance = $modal.open({
            templateUrl: '../views/officials/resident_officials.html',
            controller: $scope.officialCtrl,
            size: size

          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              window.location.reload();
            });
     }


    $scope.officialCtrl = function($http, $modalInstance, $scope, $modal){


    $scope.currentgetofficials = 1;
    $scope.getofficialsSize = 1;

      function getofficial(){
                BrgyOfficial.GetAllOfficials()
              .then(function(result){
                $scope.getofficials = result.data;
              })
      }

      getofficial();

        $scope.deleteofficials = function(id){

              if(confirm('are you sure you want to delete officials '+ id +'?') == true){

                    BrgyOfficial.DeleteOfficials(id);
                     getofficial();
                     $modalInstance.dismiss('cancel');

              }else{
                  $modalInstance.dismiss('cancel');
              };
        };

        $scope.editOfficials = function(size, id){

                  var modalInstance = $modal.open({
                    templateUrl: '../views/officials/officials_form.html',
                    controller: $scope.editofficialCtrl,
                    size: size,
                    resolve: {
                      getofficialsinfo: function(){
                               return BrgyOfficial.GetOfficials(id);
                      }
                    }

                  });

                  modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    }, function () {
                      // $log.info('Modal dismissed at: ' + new Date());
                      // window.location.reload();
                    });

        }

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        $scope.addnew =function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/officials_form.html',
                controller: $scope.addofficialsCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.clear = function(){
          $scope.showforms = false;
        }

        $scope.addofficialsCtrl = function($scope, $modalInstance, $modal){
               $scope.brgycaptain = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgycaptain = resultme.data;
                });

               $scope.brgyofficer = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgyofficer = resultme.data;
                });

                $scope.createbrgyofficials = function(){

                  $http.post(BrgyOfficialcollections+'/brgyofficial/', $scope.brgyofficers);
                  getofficial();
                  $modalInstance.dismiss('cancel');

                }

                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };

        }

        $scope.editofficialCtrl = function($modalInstance, $scope, getofficialsinfo){

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

               $scope.brgycaptain = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgycaptain = resultme.data;
                });

               $scope.brgyofficer = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgyofficer = resultme.data;
                });


            $scope.brgyofficers = getofficialsinfo.data;

            $scope.updateofficial =function(id){
              BrgyOfficial.UpdateOfficials(id, $scope.brgyofficers)
              .then(function(resultupdate){
                $scope.updateinfo = resultupdate.data;
                 getofficial();
                 $modalInstance.dismiss('cancel');
              });
            };

          $scope.deleteofficials = function(id){
                  if(confirm('are you sure you want to delete id '+ id +'?') == true){
                    BrgyOfficial.DeleteOfficials(id);
                     getofficial();
                     $modalInstance.dismiss('cancel');

                  }else{
                      $modalInstance.dismiss('cancel');
                  };
          };
        }

    };


});