'use strict';

var app = angular.module('brgyapp');

app.controller('clearanceCtrl', function($scope, $http, $rootScope, $timeout, $route, $q, Restangular, $location, $filter, ngTableParams, DbCollection) {

    var referrerId = $rootScope.UserAcount.createdbyId;

   $scope.brgyclearance = Restangular.all("clearance").getList().$object;

    $scope.delete = function(clearance) {
      clearance.remove().then(function() {

    $scope.brgyclearance = _.without($scope.brgyclearance, clearance);
    $location.path('/clearance');


     });
    };

 $http.jsonp(DbCollection + 'clearance/')
     .then(function(result){
       $scope.clearance = result.data

       var no_clearance = $scope.clearance.length
       $rootScope.no_clearance = no_clearance;

       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
      }, {
          total: $scope.clearance.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.clearance, params.orderBy()) :
                      $scope.clearance;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });
    });

       $scope.show = function(id){
          $location.url('/clearance/' + id);
        };
 });


app.controller('addclearanceCtrl', function($scope, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


     $scope.purpose = [{name: 'Lacal Employment', value: 'Lacal Employment'}];
     $scope.remark = ['No Derogatory Record'];

     $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
      });

    var referrerId = $rootScope.UserAcount.createdbyId;


      $scope.brgy_obj = function(){

        $http.get(DbCollection + 'resident/')
        .then(function(result){
        $scope.children_constituents = result.data;
        console.log($scope.children_constituents);
        return simulateAjax($scope.children_constituents);
        });

      }

	$scope.brgyclearance = Restangular.all("clearance").getList().$object;


        var simulateAjax;

        simulateAjax = function(result) {

        var deferred, fn;

        deferred = $q.defer();
        fn = function() {
          return deferred.resolve(result);
        };
        $timeout(fn, 3000);
        return deferred.promise;
      };

      $scope.brgy_obj = function(){

      $http.get(DbCollection + 'resident/')
      .then(function(result){
      $scope.children_constituents = result.data;
      console.log($scope.children_constituents);
      return simulateAjax($scope.children_constituents);
      });

      }

      $scope.brgy_obj();

      $scope.clearance = {
          children_constituents: []
      };


    $http.get(DbCollection + 'resident/')
        .then(function(result){
        return  $scope.children_constituents = result.data;
        });

    	  $scope.save = function() {

              $scope.clearance.resident = getresident.data;

              $http.post(DbCollection +'/clearance', $scope.clearance)
              .then(function(result){
                $scope.dataget = result.data;
                $modalInstance.dismiss('cancel');
                window.location.reload();
              }, function(error){
                $scope.error = error.data.message;
              });
       }


	  $scope.days = [
         {
          name: 'Lacal Employment',
          value: "Lacal Employment"
      }
		];

	$scope.pageClass = 'fadeZoom';


});

app.controller('editclearanceCtrl', function($scope, $rootScope, Clearance, $http, Restangular, $timeout, $route, $q, $location, $filter, ngTableParams, DbCollection) {

  	 $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
      });

    var referrerId = $rootScope.UserAcount.createdbyId;

		$scope.getclearance = Clearance;

       var simulateAjax;

      simulateAjax = function(result) {
        var deferred, fn;

        deferred = $q.defer();
        fn = function() {
          return deferred.resolve(result);
        };
        $timeout(fn, 3000);
        return deferred.promise;
      };

      $scope.brgy_obj = function(){

      $http.get(DbCollection + 'resident/')
      .then(function(result){
      $scope.children_constituents = result.data;
          return simulateAjax($scope.children_constituents);
      });

      }

      $scope.brgy_obj();

      $scope.clearance = {
          children_constituents: []
      };

		  $scope.clearance = Restangular.copy($scope.getclearance);



		$http.get(DbCollection + 'resident/')
	      .then(function(result){
	      return  $scope.children_constituents = result.data;
	      });

		$scope.days = [
		         {
		          name: 'Monday',
		          value: "Monday",
		      }, {
		          name: 'Tuesday',
		          value: "Tuesday"
		      }, {
		          name: 'Wednesday',
		          value: "Wednesday"
		      }, {
		          name: 'Thursday',
		          value: "Thursday"
		      }, {
		          name: 'Friday',
		          value: "Friday"
		      }, {
		          name: 'Saturday',
		          value: "Saturday"
		      }, {
		          name: 'Sunday',
		          value: "Sunday"
		      }
			];

		  $scope.isClean = function() {
		    return angular.equals($scope.clearance, $scope.clearance);
		  }

		  $scope.destroy = function() {
		    $scope.getclearance.remove().then(function() {
		      $location.path('/clearance');
		    });
		  };

		  $scope.save = function() {
		    $scope.clearance.put().then(function() {
		      $location.path('/clearance');
		    });
		  };
		  $scope.pageClass = 'fadeZoom';

});