'use strict';

var app = angular.module('brgyapp');

app.controller('businesspermitCtrl', function($scope, $modal, DbCollection, BusinessCollections, BusinessFactory, Restangular){
    $scope.businessheading = "BUSINESS LIST";

      BusinessFactory.GetAllBusiness()
      .then(function(results){
        $scope.businesslist = results.data;

        console.log($scope.businesslist);
      })

    $scope.addbusiness = function (size) {

          var modalInstance = $modal.open({
            templateUrl: '../views/businesspermit/addbusiness.html',
            controller: $scope.addbusinessCtrl,
            size: size
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {

            });
    };

    $scope.addbusinessCtrl = function($scope, $modalInstance, BusinessFactory, $http){
     $scope.formheading = "Add New Record";
      $scope.savebusiness = function(){

          BusinessFactory.CreateBusiness($scope.brgybusiness);
          console.log('e');
          $modalInstance.dismiss('cancel');
          window.location.reload();

      };

       $scope.children_me = [];
        $http.get(DbCollection + 'judicial_resident/')
              .then(function(resultme){
              $scope.children_me = resultme.data;
        });

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

    $scope.deletebusiness = function (id) {

      var con = confirm("Are you sure you want to delete this business permit?");

      if(con) {
         BusinessFactory.DeleteBusiness(id);
         window.location.reload();
      }

      else {
        return false;
      }

    };

    $scope.editbusiness = function (size, id) {


      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/addbusiness.html',
        controller: $scope.editbusinessCtrl,
        size: size,
        resolve: {
          Businesspermit: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(id);
            }else{
              return null;
            }
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.editbusinessCtrl = function($scope, $modalInstance, Businesspermit, BusinessFactory, $http){

      var id = Businesspermit.data._id;

      $scope.brgybusiness = Businesspermit.data;

      console.log($scope.brgybusiness);

      $scope.formheading = "Update Record";

      $scope.updatebusiness = function(){
          BusinessFactory.UpdateBusiness(id, $scope.brgybusiness);
          $modalInstance.dismiss('cancel');
          window.location.reload();
      };

      $scope.children_me = [];
        $http.get(DbCollection + 'judicial_resident/')
              .then(function(resultme){
              $scope.children_me = resultme.data;
        });

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

    $scope.printpermit = function (size, id) {


      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/businesspermitform.html',
        controller: $scope.printpermitCtrl,
        size: size,
        resolve: {
          Businesspermit: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(id);
            }else{
              return null;
            }
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.printpermitCtrl = function($scope, $modalInstance, Businesspermit, BusinessFactory, $http){

      var id = Businesspermit.data._id;

      $scope.businesspermit = Businesspermit.data;

      console.log($scope.businesspermit);

      $scope.formheading = "Print Permit";

      $scope.generatePermitPDF = function(){

          console.log($scope.businesspermit);

          var doc = new jsPDF();

          // use business_owner, if null, use prof_name
          $scope.businesspermit.business_owner = ($scope.businesspermit.business_owner) ? $scope.businesspermit.business_owner : $scope.businesspermit.prof_firstname + " " + $scope.businesspermit.prof_middle_name + " " + $scope.businesspermit.prof_lastname;

          // use business_name, if none given, use null
          $scope.businesspermit.business_name = ($scope.businesspermit.business_name) ? $scope.businesspermit.business_name : "";

          // use business_type, if none given, use null
          $scope.businesspermit.business_type = ($scope.businesspermit.business_type) ? $scope.businesspermit.business_type : "";

          // use business_address, if none given, use null
          $scope.businesspermit.business_address = ($scope.businesspermit.business_address) ? $scope.businesspermit.business_address : "";

          // use dateCreated, if none given, default date is set
          $scope.businesspermit.dateCreated = ($scope.businesspermit.dateCreated) ? $scope.businesspermit.dateCreated : "2015-07-28T16:00:00.000Z";
          var formatDate = new Date($scope.businesspermit.dateCreated);
          var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
          var month = String(month[formatDate.getMonth()]);
          var day = String(formatDate.getDate());
          var year = String(formatDate.getFullYear());

          var businesspermitdetails = {
              business_owner : $scope.businesspermit.business_owner,
              business_name : $scope.businesspermit.business_name,
              business_type : $scope.businesspermit.business_type,
              business_address : $scope.businesspermit.business_address,
              controlnumber : $scope.businesspermit.controlnumber,
              certificationfee : $scope.businesspermit.certificationfee,
              ornumber : $scope.businesspermit.ornumber,
              dateCreated : $scope.businesspermit.dateCreated,
              month: month,
              day: day,
              year: year
          };

          doc.setFont("Arial");
          doc.setFontSize(11);
          doc.setDrawColor(0,0,0);
          doc.setLineWidth(0.3);

          doc.text(83, 26, "Republic of the Philippines");
          doc.text(94, 31, "City of Cebu");
          doc.text(85, 36, "Barangay Basak-Pardo");

          doc.setFontSize(12);
          doc.text(77, 50, "Office of the Barangay Captain");

          doc.setFontSize(15);
          doc.text(75, 65, "BARANGAY CLEARANCE");

          doc.setFontSize(12);
          doc.text(48, 72, "Required under ARTICLE FOUR Section 152, Paragraph (C)");
          doc.text(82, 79, "(Local Government Code)");

          doc.setFontSize(9);
          doc.text(135, 100, "Control No.: " + businesspermitdetails.controlnumber);
          doc.line(153, 101, 168, 101);

          doc.setFontSize(12);
          doc.text(30, 110, "To Whom It May Concern:");

          doc.setFontSize(10);
          doc.text(30, 123, "This is to certify that ");
          doc.text(87, 123, businesspermitdetails.business_owner);
          doc.line(63, 124, 150, 124);
          doc.text(152, 123, "is legitimately");

          doc.text(30, 131, "engaged in the business of ");
          doc.line(74, 132, 140, 132);
          doc.text(90, 131, businesspermitdetails.business_type);
          doc.text(143, 131, "with a business");

          doc.text(30, 139, "style or trade name of ");
          doc.text(85, 139, businesspermitdetails.business_name);
          doc.line(66, 140, 136, 140);
          doc.text(138, 139, "located at sitio");

          doc.line(30, 148, 80, 148);
          doc.text(40, 147, businesspermitdetails.business_address);

          doc.text(30, 160, "The applicant has been residing in this Barangay since ");
          doc.line(117, 161, 132, 161);
          doc.text(134, 160, "and is known");
          doc.text(30, 168, "to me to be peace loving and law abiding.");

          doc.text(30, 180, "This is to certify further that her/his business does not encroach on any public road / street");
          doc.text(30, 185, "passage.");

          doc.text(30, 197, "Issued this");
          doc.line(49, 198, 63, 198);
          doc.text(54, 197, businesspermitdetails.day);
          doc.text(63, 197, "th day of ");
          doc.line(78, 198, 93, 198);
          doc.text(82, 197, businesspermitdetails.month);
          doc.line(94, 198, 109, 198);
          doc.text(98, 197, businesspermitdetails.year);
          doc.text(111, 197, "at Barangay Basak-Pardo, Cebu City,");
          doc.text(30, 202, "Philippines.");

          doc.text(35, 232, "Engr. Roberto A. Cabarrubias");
          doc.line(30, 233, 85, 233);
          doc.text(45, 237, "Brgy. Chairman");

          doc.setFontSize(9);
          doc.text(135, 270, "Certification Fee : " + businesspermitdetails.certificationfee);
          doc.line(160, 271, 180, 271);

          doc.text(129, 276, "Paid Under O.R. No. : " + businesspermitdetails.ornumber);
          doc.line(160, 277, 180, 277);

          doc.text(152, 282, "Date : " + businesspermitdetails.dateCreated);
          doc.line(160, 283, 180, 283);

          doc.save(businesspermitdetails.business_owner +'.pdf');

          $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

})