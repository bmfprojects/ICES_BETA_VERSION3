'use strict';

var app =angular.module('brgyapp');

app.controller('judicialCtrl', 	function($scope, $resource, $modal, $rootScope, DbCollection, ngTableParams, $http, $timeout, $q, $filter, Restangular, $location){

// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

      $scope.currentPage = 1;
      $scope.pageSize = 6;

      function getjudiciallist(){

        $http.get(DbCollection + '/judicial')
         .then(function(resultjudiciaL){
           $scope.judiciallisting = resultjudiciaL.data;
         });
      };

    getjudiciallist();

     $http.get(DbCollection + '/judicial')
     .then(function(resultjudiciaL){
       $scope.judicial = resultjudiciaL.data;
       var no_judicial = $scope.judicial.length
       $rootScope.no_judicial = no_judicial;

       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
      }, {
          total: $scope.judicial.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.judicial, params.orderBy()) :
                      $scope.judicial;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });

    });


    $scope.judicialModel = function (size) {

          var modalInstance = $modal.open({
            templateUrl: '../views/judicial.html',
            controller: $scope.judicial_addmodel,
            size: size
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

    $scope.judicial_Edit_Model = function (size, id) {
       console.log(id);
          var modalInstance = $modal.open({
            templateUrl: '../views/judicial.html',
            controller: $scope.judicial_editmodel,
            size: size,
             resolve: {
                    getjudicial: function($http){
                        if(id){
                          return $http.get(DbCollection + '/judicial/'+ id);
                        }else{
                          return null;

                        }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

    $scope.judicial_view = function (size, id) {
       console.log(id);
          var modalInstance = $modal.open({
            templateUrl: '../views/viewjudicial.html',
            controller: $scope.judicial_editmodel,
            size: size,
             resolve: {
                    getjudicial: function($http){
                        if(id){
                          return $http.get(DbCollection + '/judicial/'+ id);
                        }else{
                          return null;

                        }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

    $scope.judicial_editmodel = function($scope, getjudicial, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };



  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });


         $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.time1 = new Date();
		$scope.time2 = new Date();
		$scope.time2.setHours(7, 30);
		$scope.showMeridian = true;

    	$http.get(DbCollection + 'resident/')
	      .then(function(result){
	      return  $scope.children_constituents = result.data;
	      });


		$scope.brgyjudicial = {};

  		 $scope.brgyjudicial = getjudicial.data;
                        console.log($scope.brgyjudicial);
  		 $scope.minutes = $scope.brgyjudicial.minutes;
  		 $scope.hours = $scope.brgyjudicial.hours;

		  $scope.updatejudcial = function(judicialId, id) {

            $scope.brgyjudicial.minutes =   $scope.minutes;
            $scope.brgyjudicial.hours = $scope.hours;

                                if($scope.brgyjudicial.complainant_resident === 'No' && $scope.brgyjudicial.respondent_resident === 'Yes'){

                                      var respondent_b =   $scope.brgyjudicial.respondent.barangay.barangay_name,
                                        respondent_m    =  $scope.brgyjudicial.respondent.municipality.municipality_name,
                                        respondent_p     =  $scope.brgyjudicial.respondent.province.province_name;

                                    $scope.brgyjudicial.respondent.R_address = respondent_b +', ' + respondent_m +', ' + respondent_p;

                                      var cf =  $scope.brgyjudicial.complainant.prof_firstname ,
                                      cm     =  $scope.brgyjudicial.complainant.prof_middle_name,
                                      cl       =  $scope.brgyjudicial.complainant.prof_lastname;

                                    $scope.brgyjudicial.complainant.fullname = cf + ' ' + cm +' ' + cl;

                                        $http.put(DbCollection+'/judicial/'+judicialId, $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                            window.location.reload();
                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                         $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;

                                         $http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
                                         .then(function(result){
                                          $scope.result2 = result.data;

                                         });

                                }else if($scope.brgyjudicial.respondent_resident === 'No'  && $scope.brgyjudicial.complainant_resident === 'Yes' ){

                                      var complainant_b =   $scope.brgyjudicial.complainant.barangay.barangay_name,
                                        complainant_m    =  $scope.brgyjudicial.complainant.municipality.municipality_name,
                                        complainant_p     =  $scope.brgyjudicial.complainant.province.province_name;

                                    $scope.brgyjudicial.complainant.C_address = complainant_b +', ' + complainant_m +', ' + complainant_p;


                                      var rf =  $scope.brgyjudicial.respondent.prof_firstname ,
                                      rm     =  $scope.brgyjudicial.respondent.prof_middle_name,
                                      rl       =  $scope.brgyjudicial.respondent.prof_lastname;
                                    $scope.brgyjudicial.respondent.fullname = rf + ' ' + rm +' ' + rl;

                                        $http.put(DbCollection+'/judicial/'+judicialId, $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                            window.location.reload();
                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                }else if($scope.brgyjudicial.respondent_resident === 'No' && $scope.brgyjudicial.complainant_resident === 'No'){

                                    var rf =  $scope.brgyjudicial.respondent.prof_firstname ,
                                      rm     =  $scope.brgyjudicial.respondent.prof_middle_name,
                                      rl       =  $scope.brgyjudicial.respondent.prof_lastname;
                                    $scope.brgyjudicial.respondent.fullname = rf + ' ' + rm +' ' + rl;

                                    var cf =  $scope.brgyjudicial.complainant.prof_firstname,
                                      cm     =  $scope.brgyjudicial.complainant.prof_middle_name,
                                      cl       =  $scope.brgyjudicial.complainant.prof_lastname;

                                    $scope.brgyjudicial.complainant.fullname = cf + ' ' + cm +' ' + cl;

                                        $http.put(DbCollection+'/judicial/'+judicialId, $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                            window.location.reload();
                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                }else if($scope.brgyjudicial.respondent_resident === 'Yes'  && $scope.brgyjudicial.complainant_resident === 'Yes'){

                                      var respondent_b =   $scope.brgyjudicial.respondent.barangay.barangay_name,
                                        respondent_m    =  $scope.brgyjudicial.respondent.municipality.municipality_name,
                                        respondent_p     =  $scope.brgyjudicial.respondent.province.province_name;

                                      var complainant_b =   $scope.brgyjudicial.complainant.barangay.barangay_name,
                                        complainant_m    =  $scope.brgyjudicial.complainant.municipality.municipality_name,
                                        complainant_p     =  $scope.brgyjudicial.complainant.province.province_name;

                                    $scope.brgyjudicial.respondent.R_address = respondent_b +', ' + respondent_m +', ' + respondent_p;
                                    $scope.brgyjudicial.complainant.C_address = complainant_b +', ' + complainant_m +', ' + complainant_p;

                                        $http.put(DbCollection+'/judicial/'+judicialId, $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                            window.location.reload();
                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                         $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;

                                         $http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
                                         .then(function(result){
                                          $scope.result2 = result.data;

                                         });

                                }

		  }

		  $scope.deletejudicial = function(id){
		  	   if(confirm('are you sure you want to delete id '+ id +'?') == true){
				  	$http.delete(DbCollection+'/judicial/'+id);
				  	window.location.reload();

		      }else{
		         $modalInstance.dismiss('cancel');
		      };

		  }

               $http.get(DbCollection + 'account/')
                .then(function(result){
                $rootScope.UserAcount = result.data;
                });

$scope.judicial_pdf = function(id){

        var doc = new jsPDF('landscape');

		        $http.get(DbCollection+'/judicial/'+id)
		        .then(function(result){

		        var getdata  = result.data,

		          //uncomment below code to sae the exact filename of the pdf file.
		          //name        = prompt('Enter the Filename:'),

				// complainant info


                  prof_lastname         = getdata.complainant.prof_lastname,
                  prof_firstname        = getdata.complainant.prof_firstname,
                  prof_middle_name      = getdata.complainant.prof_middle_name,
                  C_address      = getdata.complainant.C_address,
                  municipality_name = $rootScope.UserAcount.municipality.municipality_name,
                  barangay_name = $rootScope.UserAcount.barangay.barangay_name,
                  captain = $rootScope.UserAcount.captain,
                  mlogo = $rootScope.UserAcount.mlogo,
                 // respondent info
                  respondent_prof_lastname         = getdata.respondent.prof_lastname,
                  respondent_prof_firstname        = getdata.respondent.prof_firstname,
                  respondent_prof_middle_name      = getdata.respondent.prof_middle_name,
                  R_address      = getdata.respondent.R_address,

                  duringtime                        = getdata.duringtime,
                  prof_date                        = getdata.prof_date,
                  case_type                        = getdata.case_type,
                  case_no                        = getdata.case_no,

                    case_no_tostring  = case_no.toString();
                function generateage(dateString){
                  var birthday = new Date(dateString),
                  ageDifMs = Date.now() - birthday.getTime(),
                  ageDate = new Date(ageDifMs), // miliseconds from epoch
                  get_age = Math.abs(ageDate.getFullYear() - 1970),
                  n = get_age.toString();
                  return n;
                }

                function generatedata(dateString){

                var days,
                    month;

                var d = new Date(dateString);
                days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

                var today = days[d.getUTCDay()];
                var dd = days[d.getUTCDay()];
                var mm = month[d.getMonth()]; //January is 0!
                var yyyy = d.getFullYear();
                var dy = d.getDate();

                var todays = mm+' '+dy+', '+yyyy;
                return todays;

                };

                var currentdate = new Date();
                var datetime = ""
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();



		        doc.setFontSize(12);

		        // doc.addImage(imageuri, 'JPEG', 220, 55, 50, 50);
		        // doc.addImage(respondent_imageuri, 'JPEG', 100, 55, 50, 50);

		        //Complainant Details

		        doc.setFontSize(15);
                                doc.addImage(mlogo, 'JPEG', 25, 7, 30, 30);
		        doc.text(75, 12, "Republic of the Philippines");
		        doc.text(75, 19, "Municipality/City of" + ' ' + municipality_name);
		        doc.text(75, 26, "Barangay" + ' ' + barangay_name);
                                doc.setFontSize(19);
		        doc.text(75, 37, "OFFICE OF THE LUPONG TAGAPAMAYA");

		        doc.setDrawColor(255,0,0);
		        doc.setLineWidth(1.5);
		        doc.line(20, 40, 280, 40);


		        doc.setFontSize(13);
		        doc.text(23, 100, "Complainant: ");
		        doc.setDrawColor(0,0,0);
		        doc.setLineWidth(.5);
		        doc.text(23, 108, prof_lastname + ' ' + prof_firstname + ' ' + prof_middle_name);
		        doc.line(23, 109, 70, 109);
		        doc.text(23, 117, C_address);
		        doc.line(23, 118, 70, 118);

		        // doc.addImage(respondent_imageuri, 'JPEG', 220, 45, 50, 50);
		        doc.setFontSize(13);
		        doc.text(218, 100, "Respondent/s: ");
		        doc.text(218, 108, respondent_prof_firstname + ' '  + respondent_prof_middle_name + ' '  + respondent_prof_lastname);
		        doc.line(218, 109, 265, 109);
		        doc.text(218, 117, R_address);
		        doc.line(218, 118, 265, 118);

		        doc.setFontSize(13);
		        doc.text(130, 50, "-- AGAINST --");

		        doc.text(110, 70, "Barangay Case No.: ");
		        doc.text(114, 79, case_no_tostring);
		        doc.line(112, 80, 180, 80);
		        doc.text(112, 90, "For: ");
		        doc.text(114, 99, case_type);
		        doc.line(112, 100, 180, 100);

		        var todays = new Date();

		        var getperiodof_time = new Date().toString("hh:mm tt");

				var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		        var d = Date.now();

		        var month = month[d.getMonth()];
		        var result_month = month.toString();
		        console.log(month);

		        var day = d.getDate();
		        var result_day = day.toString();
                                var result_year = todays.getFullYear();
                               var getyear_now = result_year.toString();
		        console.log(day);

		        doc.setFontSize(11);
		        doc.text(18, 130, "You are hereby summoned to appear before me in person, together with your witnesseses as scheduled below : ");

		        doc.text(50, 140, "Date :");
		        doc.text(66, 139, generatedata(prof_date));
		        doc.line(61, 140, 100, 140);
		        doc.text(105, 140, "Time :");
		        doc.text(122, 139, duringtime);
		        doc.line(118, 140, 157, 140);

		        doc.text(18, 150, "Then and there to answer to a complaint made before me, copy of which is attached hereto, for mediation or concilation of your dispute with complaint/s.");

		        doc.text(18, 155, "You are hereby warned that if you refuse or willfully fail to appear in obedience to this summons, you may be barred from filling any arising said complaint.");

		        doc.text(18, 165, "FAIL NOT or else face punishment as for contempt of court.");

		        doc.text(22, 175, "Done this");
		        doc.line(41, 175, 60, 175);
				doc.text(47, 174, result_day);
		        doc.text(62, 175, "day of");
		        doc.line(75, 175, 110, 175);
		        doc.text(83, 174, result_month);
		        doc.text(111, 175, getyear_now);

		        doc.text(200, 180, captain);
		        doc.text(190, 185, "Punong Barangay/Lupon Chairman");

		        doc.text(22, 190, "Received by:");
		        doc.line(47, 190, 90, 190);
		        doc.text(22, 196, "Date:");
		        doc.line(47, 196, 90, 196);
		        // doc.text(52, 195, generatedata(todays));

		        doc.save(prof_firstname + 'VS'+ respondent_prof_firstname +'.pdf');
		        })
  		}


    }

$scope.judicial_addmodel = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

   $scope.brgyjudicial = {};

  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });

        var currentdate = new Date();
        var datetime = {
        	hours : currentdate.getHours(),
        	minutes : currentdate.getMinutes()
        }

		$scope.minutes = datetime.minutes;
		$scope.hours = datetime.hours;

		$scope.time1 = new Date();
		$scope.time2 = new Date();
		$scope.time2.setHours(7, 30);
		$scope.showMeridian = true;

	      $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

    	$http.get(DbCollection + 'judicial_resident/')
	      .then(function(result){
	      return $scope.children_constituents = result.data;
	      });

       $http.get(DbCollection + 'account/')
          .then(function(result){
          $rootScope.UserAcount = result.data;
          });

// for the complainant demography
              $scope.brgyjudicial.complainant_demography = [{id: 'choice1'}, {id: 'choice2'}];

              $scope.addNewChoiceComplainant  = function() {
                var newItemNo = $scope.brgyjudicial.complainant_demography.length+1;
                 return $scope.brgyjudicial.complainant_demography.push({'id':'choice'+newItemNo});

              };

              $scope.removeChoiceComplainant = function() {
                var lastItem = $scope.brgyjudicial.complainant_demography.length-1;
                $scope.brgyjudicial.complainant_demography.splice(lastItem);
              };

// for the respondent demography

              $scope.brgyjudicial.Respondentdemography = [{id: 'choice1'}, {id: 'choice2'}];

              $scope.addNewChoiceRespondent = function() {
                var newItemNo = $scope.brgyjudicial.Respondentdemography.length+1;
                 $scope.brgyjudicial.Respondentdemography.push({'id':'choice'+newItemNo});

                console.log($scope.brgyjudicial.Respondentdemography)

              };

              $scope.removeChoiceRespondent = function() {
                var lastItem = $scope.brgyjudicial.Respondentdemography.length-1;
                $scope.brgyjudicial.Respondentdemography.splice(lastItem);
              };

              var currentmunicipal = $scope.UserAcount.municipality;
              console.log(currentmunicipal);

		  $scope.savejudcial = function(id) {

            $scope.brgyjudicial.minutes =   $scope.minutes;
            $scope.brgyjudicial.hours = $scope.hours;

                                if($scope.brgyjudicial.complainant_resident === 'No' && $scope.brgyjudicial.respondent_resident === 'Yes'){

                                      var respondent_b =   $scope.brgyjudicial.respondent.barangay.barangay_name,
                                        respondent_m    =  $scope.brgyjudicial.respondent.municipality.municipality_name,
                                        respondent_p     =  $scope.brgyjudicial.respondent.province.province_name;

                                    $scope.brgyjudicial.respondent.R_address = respondent_b +', ' + respondent_m +', ' + respondent_p;

                                      var cf =  $scope.brgyjudicial.complainant.prof_firstname ,
                                      cm     =  $scope.brgyjudicial.complainant.prof_middle_name,
                                      cl       =  $scope.brgyjudicial.complainant.prof_lastname;

                                    $scope.brgyjudicial.complainant.fullname = cf + ' ' + cm +' ' + cl;

                                        $http.post(DbCollection+'/judicial', $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                              window.location.reload();

                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                        $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;
                                        $http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
                                         .then(function(result){
                                          $scope.result2 = result.data;

                                         });

                                }else if($scope.brgyjudicial.respondent_resident === 'No'  && $scope.brgyjudicial.complainant_resident === 'Yes' ){

                                      var complainant_b =   $scope.brgyjudicial.complainant.barangay.barangay_name,
                                        complainant_m    =  $scope.brgyjudicial.complainant.municipality.municipality_name,
                                        complainant_p     =  $scope.brgyjudicial.complainant.province.province_name;

                                    $scope.brgyjudicial.complainant.C_address = complainant_b +', ' + complainant_m +', ' + complainant_p;

                                      var rf =  $scope.brgyjudicial.respondent.prof_firstname ,
                                      rm     =  $scope.brgyjudicial.respondent.prof_middle_name,
                                      rl       =  $scope.brgyjudicial.respondent.prof_lastname;
                                    $scope.brgyjudicial.respondent.fullname = rf + ' ' + rm +' ' + rl;

                                        $http.post(DbCollection+'/judicial', $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                              window.location.reload();

                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                }else if($scope.brgyjudicial.respondent_resident === 'No' && $scope.brgyjudicial.complainant_resident === 'No'){
                                    var rf =  $scope.brgyjudicial.respondent.prof_firstname ,
                                      rm     =  $scope.brgyjudicial.respondent.prof_middle_name,
                                      rl       =  $scope.brgyjudicial.respondent.prof_lastname;
                                    $scope.brgyjudicial.respondent.fullname = rf + ' ' + rm +' ' + rl;

                                    var cf =  $scope.brgyjudicial.complainant. prof_firstname ,
                                      cm     =  $scope.brgyjudicial.complainant.prof_middle_name,
                                      cl       =  $scope.brgyjudicial.complainant.prof_lastname;

                                    $scope.brgyjudicial.complainant.fullname = cf + ' ' + cm +' ' + cl;

                                     $http.post(DbCollection+'/judicial', $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                              window.location.reload();

                                        },function (error) {
                                                $scope.error = error.data.message;
                                         });
                                }else if($scope.brgyjudicial.respondent_resident === 'Yes'  && $scope.brgyjudicial.complainant_resident === 'Yes'){

                                      var respondent_b =   $scope.brgyjudicial.respondent.barangay.barangay_name,
                                        respondent_m    =  $scope.brgyjudicial.respondent.municipality.municipality_name,
                                        respondent_p     =  $scope.brgyjudicial.respondent.province.province_name;

                                      var complainant_b =   $scope.brgyjudicial.complainant.barangay.barangay_name,
                                        complainant_m    =  $scope.brgyjudicial.complainant.municipality.municipality_name,
                                        complainant_p     =  $scope.brgyjudicial.complainant.province.province_name;

                                    $scope.brgyjudicial.respondent.R_address = respondent_b +', ' + respondent_m +', ' + respondent_p;
                                    $scope.brgyjudicial.complainant.C_address = complainant_b +', ' + complainant_m +', ' + complainant_p;

                                        $http.post(DbCollection+'/judicial', $scope.brgyjudicial)
                                        .then(function(result){
                                            $scope.result = result.data;
                                            $modalInstance.dismiss('cancel');
                                              window.location.reload();

                                        },function (error) {
                                            $scope.error = error.data.message;
                                        });

                                        $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;
                                        $http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
                                         .then(function(result){
                                          $scope.result2 = result.data;

                                         });
                                }



		  }
    }

});


