'use strict';

var app = angular.module('brgyapp');

app.controller('residentCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, ReportService){
  // var get_ID_Format = 'data:image/jpg;base64,'+ItemsServices.id_format();
 // console.log(get_ID_Format);
 $scope.searchtypes = ItemsServices.searchType();
  $scope.currentPage = 1;
  $scope.pageSize = 6;

  function getresidentlist(){

    $http.get(DbCollection + 'resident/')
     .then(function(result){
       return $scope.residents = result.data;
          var people = $scope.residents.length;
          return $rootScope.people = people;
     });

  }

$scope.allview = function(){
getresidentlist();
// window.location.reload();
}
    $scope.$watch('searchtypes.searchtype', function(Stype){
      if(Stype){
          $scope.stype = Stype;
          if($scope.stype.searchName == 'Gender'){
                 $scope.GenderTypes = ItemsServices.GenderType();
                 $scope.$watch('GenderTypes.gendername', function(getgender){
                   if(getgender){
                          return ResidentServices.GetSpecificGender(getgender.gendername)
                          .success(function(data){
                            $scope.residents = data;
                                   var people = $scope.residents.length;
                                   $rootScope.people = people;
                          });
                   }

                  });

          }else if($scope.stype.searchName == 'Full Name'){

                  $http.get(DbCollection + 'resident/')
                   .then(function(result){
                      $scope.residentSearch = result.data;
                   })

                  $scope.name="";
                  $scope.onItemSelected = function(fullname){
                         return ResidentServices.GetSpecificFullname(fullname)
                          .success(function(data){
                            $scope.residents = data;
                                   var people = $scope.residents.length;
                                   $rootScope.people = people;
                          });
                  }

          }else if($scope.stype.searchName == 'Blood Type'){

            $scope.bloodtypesearch = ItemsServices.bloodtype();
                 $scope.$watch('bloodtypesearch.blood_type', function(getbloodType){
                   if(getbloodType){
                          return ResidentServices.GetSpecificBloodType(getbloodType.blood_type)
                          .success(function(data){
                            $scope.residents = data;
                                   var people = $scope.residents.length;
                                   $rootScope.people = people;
                          });
                   }

                  });

          }else if($scope.stype.searchName == 'Health Service Availed'){

            $scope.healthSearch = ItemsServices.healthAvailed();
            console.log($scope.healthSearch);
                 $scope.$watch('healthSearch.name', function(healthsAvailed){
                   if(healthsAvailed){
                          return ResidentServices.GetSpecifichealthAvailed(healthsAvailed.name)
                          .success(function(data){
                            $scope.residents = data;
                                   var people = $scope.residents.length
                                   $rootScope.people = people;
                          });
                   }

                  });

          }else if($scope.stype.searchName == 'Occupation'){

                  $http.get(DbCollection + 'resident/')
                   .then(function(result){
                      $scope.residentSearch = result.data
                   })

                  $scope.name="";
                  $scope.onItemSelected = function(prof_occupation){
                         return ResidentServices.GetSpecificOccupation(prof_occupation)
                          .success(function(data){
                            $scope.residents = data;
                                   var people = $scope.residents.length
                                   $rootScope.people = people;
                          });
                  }

          }

      }else{
            getresidentlist();
        }

    });

  $scope.calculate_age = function(d){
        var birthday = new Date(d);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getFullYear() - 1970);
  };

// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

 $http.get(DbCollection + 'brgy_userlist/')
     .then(function(result){
       $scope.useraccount = result.data
      $rootScope.account_count = $scope.useraccount.length
  });

    $http.get(DbCollection + 'resident/')
     .then(function(result){
       $scope.resident = result.data
       var people = $scope.resident.length
       $rootScope.people = people;

       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page

      }, {
          total: $scope.resident.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.resident, params.orderBy()) :
                      $scope.resident;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });
    });


    $scope.destroy = function (size, id, f, m, l) {

          var modalInstance = $modal.open({
            templateUrl: '../views/deleteresident.html',
            controller: $scope.deleteme,
            size: size,
             resolve: {
                    getdelete: function(){
                           var contentdata = {
                            id : id,
                            f : f,
                            m : m,
                            l : l
                           };
                           return contentdata;
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };
 $scope.deleteme = function($scope, $modalInstance, getdelete, $modal) {
  $scope.getdelete = getdelete;
      $scope.yes = function(id, f, m, l){
                  $http.delete(DbCollection+'/resident/'+ id)
                  getresidentlist();
                    $modalInstance.dismiss('cancel');

        };
      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
  }

  $scope.show = function(id){
    $location.url('/resident/' + id);
  };

    $scope.residentModel = function (size) {

          var modalInstance = $modal.open({
            templateUrl: '../views/resident.html',
            controller: $scope.addmodel,
            size: size
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };


          $scope.resident_edit_Model = function (size, id) {

            var modalInstance = $modal.open({
              templateUrl: '../views/resident.html',
              controller: $scope.model,
              size: size,
              resolve: {
                    getresident: function($http){
                        if(id){
                          return $http.get(DbCollection + '/resident/'+ id);
                        }else{
                          return null;

                        }
                      }
                    }
            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
              });

      };

  $scope.addmodel = function($scope, $modalInstance, $modal) {

$scope.getbaranggay = [];
$http.get(DbCollection + 'brgycol/')
  .then(function(places) {
     $scope.getbaranggay = places.data;
    }, function(error) {
      alert('Failed: ' + error);
    });

console.log($scope.getbaranggay );
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

 $scope.children_me = [];
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children = [];

    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });

$scope.children_m = []
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });

              $scope.$watch('brgyresident.b_date', function(dateString){
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

          $http.get(DbCollection+'/region')
          .then(function(result){
            $scope.r = result.data;
          });

          $scope.$watch('brgyresident.region_bplace._id', function(id){
              $http.get(DbCollection+'/province/'+id)
              .then(function(result){
               $scope.p = result.data;
              })
          });

          $scope.$watch('brgyresident.province_bplace._id', function(id){
              $http.get(DbCollection+'/municipality/'+id)
              .then(function(result){
               $scope.m = result.data;
              })
          });

          $scope.$watch('brgyresident.municipality_bplace._id', function(id){
              $http.get(DbCollection+'/brgycol/'+id)
              .then(function(result){
               $scope.b = result.data;
              })
          });

          $scope.showImageCropper = true;

          var currentmunicipal = $scope.UserAcount.municipality;

          $scope.brgyresident = {
                  upload: []
           };

          $scope.uploadFile = function(){
                var file = $scope.avatar;
                var uploadUrl = "resident/images";
                fileUpload.uploadFileToUrl(file, uploadUrl);
            };

          $scope.brgyresident = {
                    avatar: []
           };

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();

        var img;
                 $scope.take_snapshot = function(data_uri) {
                        Webcam.snap( function(data_uri) {
                            $scope.brgyresident.imageuri = data_uri;
                            document.getElementById('capture_image').innerHTML =
                                '<img id="myImg" src="'+data_uri+'"/>' +
                                '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+
                                $scope.brgyresident.imageuri +'" >';
                        });

                            var x = document.getElementById("myImg").src;
                            var x_result = x.toString();
               }

              $scope.brgyresident.demography = [{id: 'choice1'}, {id: 'choice2'}];

              $scope.addNewChoice = function() {
                var newItemNo = $scope.brgyresident.demography.length+1;
                 $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

                console.log($scope.brgyresident.demography)

              };

              $scope.removeChoice = function() {
                var lastItem = $scope.brgyresident.demography.length-1;
                $scope.brgyresident.demography.splice(lastItem);
              };

                $scope.saveresident = function() {
                 var bdate = $scope.brgyresident.b_date;
                  var dbmonth = ItemsServices.getmonth(bdate);
                  var bvalue = ItemsServices.getdatevalue(bdate);
                  var byear = ItemsServices.get_issuedYear(bdate);

                  $scope.brgyresident.fullname =  $scope.brgyresident.prof_firstname+' '+
                                                                      $scope.brgyresident.prof_middle_name+' '+
                                                                      $scope.brgyresident.prof_lastname;

                  $scope.brgyresident.birthvalue = bvalue;
                  $scope.brgyresident.birthmonth = dbmonth;
                  $scope.brgyresident.birthyear = byear;
                  $scope.brgyresident.birthString= dbmonth +' '+ bvalue +', '+ byear;

                Restangular.all('resident').post($scope.brgyresident).then(function(brgyresident) {
                  getresidentlist();
                   $modalInstance.dismiss('cancel');
                }, function (error) {
                        $scope.error = error.data.message;
                    });
              }

      }

      $scope.viewresidentModel = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.model,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;

                      }
                    }
                  }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              window.location.reload();
            });

    };

$scope.model = function($scope, $modalInstance, $modal, getresident) {

$scope.getbaranggay = [];
$http.get(DbCollection + 'brgycol/')
  .then(function(places) {
     $scope.getbaranggay = places.data;
    }, function(error) {
      alert('Failed: ' + error);
    });

$scope.children = {};
var resident_childs = $resource(DbCollection + '/resident');
$scope.v = 7;
$scope.zv = 50;
$scope.z = 50;

  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

 $scope.children_me = [];
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
        $scope.children = {};
$scope.children_d = [];
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });

$scope.children_m = [];
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });

var today = Date.now();

        function generatedata(today){

        var days,
            month;

        var d = new Date(today);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();

        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

var getdate_now = generatedata(today);

var getperiodof_time = new Date().toString("hh:mm tt");

var url_value =  getresident.data.shortIds;

//Generate the QRCode for every full name of the resident
    var qrcode = new QRCode("qrcode", {
                     width : 200,
                     height : 200
                   });

    var sss = qrcode.makeCode(url_value);

              $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.brgyresident.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+
                        $scope.brgyresident.imageuri +'" >';
                });
                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
            }


           $scope.$watch('brgyresident.b_date', function(dateString){
                  var birthday = new Date(dateString);
                  var ageDifMs = Date.now() - birthday.getTime();
                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
                  $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

            });


      $scope.currentPage = 1;
      $scope.pageSize = 20;
      $scope.clearance = [];

      function getresidentclearancerecord() {

            return ResidentServices.GetClearance(getresident.data._id)
            .then(function(result){
             return $scope.clearance = result.data;
            });
      }

      getresidentclearancerecord();

          $http.get(DbCollection+'/region')
          .then(function(result){
            $scope.r = result.data;
          });

          $scope.$watch('brgyresident.region_bplace._id', function(id){
              $http.get(DbCollection+'/province/'+id)
              .then(function(result){
               $scope.p = result.data;
              })
          });

          $scope.$watch('brgyresident.province_bplace._id', function(id){
              $http.get(DbCollection+'/municipality/'+id)
              .then(function(result){
               $scope.m = result.data;
              })
          });

          $scope.$watch('brgyresident.municipality_bplace._id', function(id){
              $http.get(DbCollection+'/brgycol/'+id)
              .then(function(result){
               $scope.b = result.data;
              })
          });

          $scope.create_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.clearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/clearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
               getresidentclearancerecord();
            });

        };

        $scope.edit_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              getresidentclearancerecord();
            });

        };

        $scope.show_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewclearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
               getresidentclearancerecord();
               window.location.reload();
            });

        };

          $scope.editclearanceCtrl = function($scope, $modalInstance, getclearance, $modal) {
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

            $scope.clearance = getclearance.data;

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

            $scope.update_clearance = function(cid, rid) {

            $http.get(DbCollection + '/resident/'+ rid)
            .then(function(resultr){
              $scope.clearance.resident = resultr.data;
                  $scope.clearance.datevalue = ItemsServices.getdatevalue($scope.clearance.jud_issuedON);
                  $scope.clearance.dateYear = ItemsServices.get_issuedYear($scope.clearance.jud_issuedON);
                  $scope.clearance.specificday = ItemsServices.getday($scope.clearance.jud_issuedON);
                  $scope.clearance.specificmonth = ItemsServices.getmonth($scope.clearance.jud_issuedON);

               $http.put(DbCollection+'/clearance/'+cid, $scope.clearance)
               .then(function(result) {
                    getresidentclearancerecord();
                    $modalInstance.dismiss('cancel');
                  }, function(error){
                    $scope.error = error.data.message;
                  });

            });


              };

          }

          $scope.clearanceCtrl = function($scope, $modalInstance, $modal) {

               $http.get(DbCollection + 'account/')
                .then(function(result){
                $rootScope.UserAcount = result.data;
                });

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
              var referrerId = $rootScope.UserAcount.createdbyId;

            $scope.brgyclearance = Restangular.all("clearance").getList().$object;

              $http.get(DbCollection + 'resident/')
                  .then(function(result){
                  return  $scope.children_constituents = result.data;
                  });

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

            $scope.save = function() {
                  var residentname = getresident.data;
                  $scope.clearance.resident = getresident.data;
                  $scope.clearance.datevalue = ItemsServices.getdatevalue($scope.clearance.jud_issuedON);
                  $scope.clearance.dateYear = ItemsServices.get_issuedYear($scope.clearance.jud_issuedON);
                  $scope.clearance.specificday = ItemsServices.getday($scope.clearance.jud_issuedON);
                  $scope.clearance.specificmonth = ItemsServices.getmonth($scope.clearance.jud_issuedON);
                  $scope.clearance.residentName = residentname.prof_firstname +' '+ residentname.prof_middle_name +' '
                  +residentname.prof_lastname ;

                  console.log($scope.clearance.dateYear );
                  console.log($scope.clearance.specificday);
                  console.log($scope.clearance.specificmonth);

                  console.log($scope.clearance.datevalue);
                  $http.post(DbCollection +'/clearance', $scope.clearance)
                  .then(function(result){
                     getresidentclearancerecord();
                    $modalInstance.dismiss('cancel');

                  }, function(error){
                    $scope.error = error.data.message;
                  });
            }
          }

            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                   = ItemsServices.healths();
            $scope.others                     = ItemsServices.others();
            $scope.waters                    = ItemsServices.waters();
            $scope.supplies                  = ItemsServices.supplies();
            $scope.facilities                  = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

        $scope.download_pdf = function(id){

        var doc = new jsPDF('landscape');

        $http.get(DbCollection+'/getclearance/'+id)
        .then(function(result){
        var mlogo = $rootScope.UserAcount.mlogo;
        console.log(mlogo);
        var getdata     = result.data,

          //uncomment below code to sae the exact filename of the pdf file.
          // name        = prompt('Enter the Filename:'),
          baranggay_bplace         = getdata.resident.baranggay_bplace,
          prof_lastname             = getdata.resident.prof_lastname,
          prof_firstname        = getdata.resident.prof_firstname,
          prof_middle_name      = getdata.resident.prof_middle_name,
          prof_status           = getdata.resident.prof_status,
          prof_gender           = getdata.resident.prof_gender,
          b_date                = getdata.resident.b_date,
          region                = getdata.resident.region,
          province              = getdata.resident.province,
          captain               = getdata.resident.captain,
          municipality          = getdata.resident.municipality,
          date                  = getdata.date,
          jud_issuedAT          = getdata.jud_issuedAT,
          restCert_num          = getdata.restCert_num,
          ctc_num               = getdata.ctc_num,
          date_of_issue         = getdata.date_of_issue,
          clearance_control_no  = getdata.clearance_control_no,
          clearnce_no           = getdata._id,
          purpose               = getdata.purpose,
          imageuri              = getdata.resident.imageuri,
          remark                = getdata.remark,
          b_date                = getdata.resident.b_date,
          shortids              =getdata.shortids,
          barangay              = getdata.resident.barangay,
          municipalitylogo = mlogo;

        function generateage(dateString){
          var birthday = new Date(dateString),
          ageDifMs = Date.now() - birthday.getTime(),
          ageDate = new Date(ageDifMs), // miliseconds from epoch
          get_age = Math.abs(ageDate.getFullYear() - 1970),
          n = get_age.toString();
          return n;
        }

        function generatedata(dateString){

        var days,
            month;

        var d = new Date(dateString);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();

        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

        var currentdate = new Date();
        var datetime = ""
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

        doc.addImage(municipalitylogo, 'JPEG', 40, 7, 30, 30);
        doc.setFontSize(13);
        doc.text(75, 15, "Republic of the Philippines");
        doc.text(75, 20, 'Municipality/City of ' + municipality.municipality_name);
        // doc.setFontSize(15);
        // doc.setFontType("bold");
        doc.text(75, 25, 'Sangguniang Barangay ng ' + barangay.barangay_name);

        //obove red line
        doc.setDrawColor(0,0,255);
        doc.setLineWidth(1);
        doc.line(75, 30, 275, 30);

        doc.setDrawColor(255,0,0);
        doc.setLineWidth(1);
        doc.line(75, 32, 275, 32);
        // doc.setDrawColor(0,0,0);
        // doc.setLineWidth(.5);
        // doc.line(215, 140, 275, 140);

        doc.setDrawColor(255,0,0);
        doc.setLineWidth(1.5);
        doc.line(20, 170, 280, 170);

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.5);
        doc.line(215, 140, 275, 140);


        doc.setDrawColor(0,0,0);
        doc.setFontSize(19);
        doc.setFont("helvetica");

        doc.setFontType("bold");
        doc.text(95, 40, ' B A R A N G A Y   C L E A R A N C E ');

        doc.setFontSize(11);
        doc.setFontType("italic");
        doc.text(35, 50, 'TO WHOM IT MAY CONCERN:');

        doc.setFontSize(11);
        doc.setFont("helvetica");
        doc.text(50, 60, 'This is to certify that the person whose name, picture and signature appear hereon');
        doc.text(35, 65, 'has requested a CERTIFICATION from this office and the result/s is/are listed below:');
        doc.setFontSize(11);
        doc.text(35, 75, 'NAME');
        doc.text(35, 80, 'ADDRESS');
        doc.text(35, 85, 'GENDER');
        doc.text(35, 90, 'CIVIL STATUS');
        doc.text(35, 95, 'BIRTH-DATE');
        doc.text(35, 100, 'BIRTH-PLACE');
        doc.text(35, 105, 'AGE');

        doc.text(70, 75, ' : ');
        doc.text(70, 80, ' : ');
        doc.text(70, 85, ' : ');
        doc.text(70, 90, ' : ');
        doc.text(70, 95, ' : ');
        doc.text(70, 100, ' : ');
        doc.text(70, 105, ' : ');


        doc.text(75, 75, prof_firstname + ' ' + prof_middle_name + ' ' + prof_lastname);
        doc.text(75, 80, barangay.barangay_name +', '+ municipality.municipality_name + ', '
          + province.province_name);
        doc.text(75, 85, prof_gender);
        doc.text(75, 90, prof_status);
        doc.text(75, 95, generatedata(b_date));
        doc.text(75, 100, baranggay_bplace);
        // doc.text(75, 100, barangay_bplace.barangay_name +', '+ municipality_bplace.municipality_name + ', '
          // + province_bplace.province_name);
        doc.text(75, 105, generateage(b_date));

        var todays = new Date();

        doc.setFont("helvetica")
        doc.text(35, 115, 'Given this');
        doc.text(35, 120, 'Remark');
        doc.text(35, 125, 'Purpose');


        doc.text(70, 115, ' : ');
        doc.text(70, 120, ' : ');
        doc.text(70, 125, ' : ');
        doc.text(70, 130, ' : ');
        doc.text(70, 135, ' : ');
        doc.text(70, 140, ' : ');
        doc.text(70, 145, ' : ');
        doc.text(70, 145, ' : ');

        var getperiodof_time = new Date().toString("hh:mm tt");


        doc.text(75, 115, generatedata(todays));
        doc.text(75, 120, remark);
        doc.text(75, 125, purpose);
        doc.text(75, 130, clearance_control_no);
        doc.text(75, 135, generatedata(date_of_issue));
        doc.text(75, 140, barangay.barangay_name +', '+ municipality.municipality_name + ', '
          + province.province_name);
        doc.text(75, 145, getperiodof_time);

        doc.text(220, 145, 'SIGNATURE OF APPLICANT');

        doc.text(190, 50, 'CLEARANCE ID : '+ shortids);
        doc.text(35, 130, 'Tax Cert. Serial');
        doc.text(35, 135, 'Issued on');
        doc.text(35, 140, 'Issued at');
        doc.text(35, 145, 'Time');

        doc.setFontType('bolditalic');
        doc.text(140, 160, captain);
        doc.setFontType('italic');
        doc.text(140, 165, 'PUNONG BARANGAY');
        doc.text(35, 175, 'Note: Not valid without official dry seal. This Barangay Clearance is valid for 1 year from date of issue.');


        var idgetbase = document.getElementById('basegetme').src;
        var idgetbaseme = document.getElementById('imageme').src;

        doc.addImage(idgetbaseme, 'JPEG', 220, 55, 50, 50);
        doc.addImage(idgetbase, 'PNG', 239, 172, 20, 20);

        doc.save(prof_firstname +'.pdf');


        });

  }
        $scope.childrens = {};
        $scope.childrens = [];

        // $scope.brgyresident.demography = [{id: 'choice1'}, {id: 'choice2'}];

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

        // $scope.removeChoice = function() {
        //   var lastItem = $scope.brgyresident.demography.length-1;
        //   $scope.brgyresident.demography.splice(lastItem);
        // };

            $scope.removeChoice = function(item){
               $scope.brgyresident.demography.splice($scope.brgyresident.demography.indexOf(item),1);
            }

        $scope.updateresident = function(id) {

                  var bdate = $scope.brgyresident.b_date;
                  var dbmonth = ItemsServices.getmonth(bdate);
                  var bvalue = ItemsServices.getdatevalue(bdate);
                  var byear = ItemsServices.get_issuedYear(bdate);

                  $scope.brgyresident.fullname = $scope.brgyresident.prof_firstname+' '+
                                                                      $scope.brgyresident.prof_middle_name+' '+
                                                                      $scope.brgyresident.prof_lastname;
                                                                      console.log($scope.brgyresident);

                  $scope.brgyresident.birthvalue =bvalue;
                  $scope.brgyresident.birthmonth = dbmonth;
                  $scope.brgyresident.birthyear = byear;
                  $scope.brgyresident.birthString= dbmonth +' '+ bvalue +', '+ byear;

          $http.put(DbCollection + '/resident/'+ id, $scope.brgyresident)
          .then(function (result) {
                $scope.brgyresident = result.data;
                getresidentlist();
                $modalInstance.dismiss('cancel');
          }, function (error) {
                $scope.error = error.data.message;
          });

        };

        $scope.destroy = function(id){

            if(confirm('are you sure you want to delete id '+ id +'?') == true){
                $http.delete(DbCollection+'/resident/'+id)
                getresidentlist();
                $modalInstance.dismiss('cancel');

            }else{
                $modalInstance.dismiss('cancel');
            };
        };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }



    }
  $scope.view_guest = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.viewCtrl,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;

                      }
                    }
                  }
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              window.location.reload();
            });

    };

 $scope.viewCtrl = function($scope, $modalInstance, $modal, getresident) {

$scope.zv = 50;
$scope.v = 7;
$scope.url_value = DbCollection+'viewresident/'+ getresident.data._id;

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }
 }

  $scope.generatethisID = function(id){

    var doc = new jsPDF();
    var get_ID_Format5  = 'data:image/jpg;base64,'+ItemsServices.id_format7();
    var get_ph_flag = 'data:image/png;base64,'+ItemsServices.philippinesflag();

    ResidentServices.GetResident(id)
    .then(function(resultdata){
      var getresidentid;
      var getresidentID = resultdata.data;
      var mlogo = $rootScope.UserAcount.mlogo;

                  getresidentid = {
                            _id :  getresidentID._id,
                          birthString: getresidentID.birthString,
                          captain: getresidentID.captain,
                          imageuri: getresidentID.imageuri,
                          prof_gender: getresidentID.prof_gender,
                          fullname: getresidentID.fullname,
                          firstname: getresidentID.prof_firstname,
                          middlename: getresidentID.prof_middle_name,
                          lastname: getresidentID.prof_lastname,
                          barangay_name: getresidentID.barangay.barangay_name,
                          municipality: getresidentID.barangay.municipality,
                          province: getresidentID.barangay.province,
                          shortids: getresidentID.shortIds,
                          mlogo: mlogo,
                          idqrcode : qr.toDataURL(getresidentID.shortIds)
                  };

                        // doc.setFontType("bold");
                        doc.setFont("Arial");
                        doc.setFontSize(6.7);

                        // ID wrapper .. 1st 2 numbers is X and Y (position).. last 2 numbers is for the img size..
                        doc.addImage(get_ID_Format5, 'JPG', 17, 20, 87, 53);

                        doc.setTextColor(0);

                        doc.text(39, 26, "Republic of the Philippines");
                        doc.text(39, 29, 'Municipality/City of ' + getresidentid.municipality);
                        doc.text(39, 32, 'Sangguniang Barangay ng ' + getresidentid.barangay_name);
                        doc.addImage(getresidentid.idqrcode, 'JPEG', 82, 39, 17, 17);
                        doc.addImage(get_ph_flag, 'PNG', 23, 25, 13, 7);
                        doc.addImage(getresidentid.imageuri, 'PNG', 60, 38, 17, 17);

                        doc.setFontSize(6.5);
                        doc.addImage(getresidentid.mlogo, 'JPEG', 85, 24, 9, 9);
                        doc.text(79, 63, 'ID: '+getresidentid.shortids);
                        doc.text(45, 63, getresidentid.captain);
                        doc.text(42, 63, ' : ');
                        doc.text(25, 63, 'Brgy. Captain');

                        doc.text(23, 41, ' Firstname ');
                        doc.text(23, 44, ' Middlename ');
                        doc.text(23, 47, ' Lastname ');
                        doc.text(23, 50, ' Gender ');
                        doc.text(23, 53, ' Birthdate ');

                        doc.text(36, 41, ' : ');
                        doc.text(36, 44, ' : ');
                        doc.text(36, 47, ' : ');
                        doc.text(36, 50, ' : ');
                        doc.text(36, 53, ' : ');

                        doc.text(39, 41, getresidentid.firstname);
                        doc.text(39, 44, getresidentid.middlename);
                        doc.text(39, 47, getresidentid.lastname);
                        doc.text(39, 50, getresidentid.prof_gender);
                        doc.text(39, 53, getresidentid.birthString);

                        doc.save(getresidentid.firstname+'_ID.pdf');

    });

  };

});

app.controller('viewresidentCtrl', function($scope, getbrgyresident, $rootScope,
  $modal,
  $http, Restangular, ngTableParams, $q, $filter, DbCollection, $location){
 $scope.brgyresident =  getbrgyresident;
 console.log($scope.brgyresident);
});
