'use strict';

var app = angular.module('brgyapp');

app.controller('OfficialsaccountCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, Signupviewers,  BrgyOfficialcollections, BrgyOfficial, ReportService){

        function getofficialsaccount(){
                $http.get(Signupviewers + '/viewers')
                .then(function(result){
                 $scope.residentofficialsaccount = result.data;
                })
        }

        getofficialsaccount();

        $scope.addofficialaccount = function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.addOfficialsAccountCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.addOfficialsAccountCtrl = function($scope, $modalInstance, $modal){

            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            };

           $scope.registerOfficial = function () {
            $http.post(Signupviewers + '/viewers', $scope.account)
            .then(function (result) {
                    $scope.account = result.data[0];
                    getofficialsaccount();
                    $modalInstance.dismiss('cancel');
                    $rootScope.message_access = "Official acccount will now have access: '"+ $scope.account.username + "' this user can login as viewer.";

            }, function (error) {
                    $scope.error = error.data.message;
                    console.log($scope.account);
                });
            };
        }

      $scope.editofficialaccount = function(size, id){
            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.EditOfficialsAccountCtrl,
                size: size,
                resolve: {
                  getaccount: function($http){
                      return $http.get(Signupviewers+'/viewers/'+ id);
                  }
                }
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });

      }

        $scope.EditOfficialsAccountCtrl = function($modalInstance, $modal, $scope, getaccount, $http){

              $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
              };

              $scope.account = getaccount.data;

              $scope.updateofficials = function(id){
                $http.put(Signupviewers+ '/viewers/'+ id, $scope.account)
                .then(function(result){
                  $scope.resultdata = result.data;
                      getofficialsaccount();
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                      $scope.error = error.data.message;
                      console.log($scope.account);
                  });
              };
        };

        $scope.currentgetofficials = 1;
        $scope.getofficialsSize = 1;

          function getofficial(){
                  BrgyOfficial.GetAllOfficials()
                  .then(function(result){
                   return $scope.getofficials = result.data;
                  })
          }

          getofficial();

            $scope.deleteofficials = function(id){

                        if(confirm('are you sure you want to delete officials '+ id +'?') == true){

                              BrgyOfficial.DeleteOfficials(id);
                               getofficial();


                        }else{
                            $modalInstance.dismiss('cancel');
                        };
                  };

                  $scope.editOfficials = function(size, id){

                            var modalInstance = $modal.open({
                              templateUrl: '../views/officials/officials_form.html',
                              controller: $scope.editofficialCtrl,
                              size: size,
                              resolve: {
                                getofficialsinfo: function(){
                                         return BrgyOfficial.GetOfficials(id);
                                }
                              }

                            });

                            modalInstance.result.then(function (selectedItem) {
                              $scope.selected = selectedItem;
                              }, function () {
                                // $log.info('Modal dismissed at: ' + new Date());
                                // window.location.reload();
                              });

                  }

                  $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                  };

        $scope.addnew =function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/officials_form.html',
                controller: $scope.addofficialsCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.clear = function(){
          $scope.showforms = false;
        }

        $scope.addofficialsCtrl = function($scope, $modalInstance, $modal){
               $scope.brgycaptain = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgycaptain = resultme.data;
                });

               $scope.brgyofficer = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgyofficer = resultme.data;
                });

                // $scope.$watch('', function(resultlength){
                //     var kagawad_no =  resultlength;
                //     console.log(kagawad_no);
                // });


                $scope.createbrgyofficials = function(){

                  $http.post(BrgyOfficialcollections+'/brgyofficial/', $scope.brgyofficers);
                  getofficial();
                  $modalInstance.dismiss('cancel');

                }

                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };

        }

        $scope.editofficialCtrl = function($modalInstance, $scope, getofficialsinfo){

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

               $scope.brgycaptain = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgycaptain = resultme.data;
                });

               $scope.brgyofficer = [];
                $http.get(DbCollection + 'resident/')
                      .then(function(resultme){
                      $scope.brgyofficer = resultme.data;
                });


            $scope.brgyofficers = getofficialsinfo.data;

            $scope.updateofficial =function(id){
              BrgyOfficial.UpdateOfficials(id, $scope.brgyofficers)
              .then(function(resultupdate){
                $scope.updateinfo = resultupdate.data;
                 getofficial();
                 $modalInstance.dismiss('cancel');
              });
            };

          $scope.deleteofficials = function(id){
                  if(confirm('are you sure you want to delete id '+ id +'?') == true){
                    BrgyOfficial.DeleteOfficials(id);
                     getofficial();
                     $modalInstance.dismiss('cancel');

                  }else{
                      $modalInstance.dismiss('cancel');
                  };
          };
        }
})