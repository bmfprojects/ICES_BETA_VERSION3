'use strict';
var app = angular.module('brgyapp',
	[
		'ngResource',
		'ngRoute',
		'restangular',
		'ui.bootstrap',
		'angularUtils.directives.dirPagination',
		'monospaced.qrcode',
		'ngMessages',
		'ngTable',
		'ui.timepicker',
                        'ResidentService',
                        'brgyOfficials',
                        'BusinessFactories',
                        'viewReportServices',
		'ngUpload',
		'ngSanitize',
		'ui.select',
		'angularFileUpload',
		'ngCookies',
		'ngTagsInput',
		'checklist-model',
                      'angular-loading-bar'
	]);

// get the hostname and port from the server
// ==========================================

var hostname 	= window.location.hostname;
var port 	= window.location.port;
var api 		= hostname + ':' + port;

//===========================================

app.constant('BrgyOfficialcollections', 'http://'+ api +'/brgyofficials/');
app.constant('SearchBaseurl', 'http://'+ api +'/search/');
app.constant('DbCollection', 'http://'+ api +'/api/');
app.constant('loginCollection', 'http://'+ api +'/users');
app.constant('Signupviewers', 'http://'+ api +'/viewer');
app.constant('BaranggayCollections', 'http://'+ api +'/brgycol');
app.constant('BusinessCollections', 'http://'+ api +'/businesspermits');

app.config(function($routeProvider, $locationProvider, cfpLoadingBarProvider,  RestangularProvider, DbCollection){
    // cfpLoadingBarProvider.includeSpinner = false;
    // cfpLoadingBarProvider.includeBar = false;
    // cfpLoadingBarProvider.latencyThreshold = 500;
    $locationProvider.html5Mode(true);

	$routeProvider

	.when('/login', {
		templateUrl: 'main.html',
		controller: 'navbarCtrl'
	})
	.when('/logout', {
		controller: 'logoutCtrl',
		resolve:{
			 auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/', {
		templateUrl: 'main.html',
		controller: 'navbarCtrl'
	})

	.when('/home', {
		templateUrl: 'main2.html',
		controller: 'residentCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}

	})

	.when('/region', {
		templateUrl: 'views/region/regionlist.html',
		controller: 'regionCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                          var userInfo = authenticationSvc.getUserInfo();
                          if (userInfo) {
                              return $q.when(userInfo);
                          } else {
                              return $q.reject({ authenticated: false });
                          }
                      }
		}
	})
	.when('/region/create', {
		templateUrl: 'views/region/region.html',
		controller: 'addregionCtrl',
		resolve:{
			 auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/region/:id', {
		templateUrl: 'views/region/region.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          }
	        }
	})

	.when('/viewprovinces/:id', {
		templateUrl: 'views/region/viewprovince.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/region/createprovince/:id', {
		templateUrl: 'views/region/province.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})

	.when('/viewmunicipality/:id', {
		templateUrl: 'views/region/municipalitylist.html',
		controller: 'provinceCtrl',
		    resolve: {
	          Province: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'getprovince/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})

   .when('/viewbrgydata/:id', {
		templateUrl: 'views/region/brgylist.html',
		controller: 'barangayCtrl',
		    resolve: {
	          Municipality: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'/getmunicipality/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
	            }
	        }
	})

   .when('/addbrgydata/:id', {
		templateUrl: 'views/region/brgy.html',
		controller: 'addbarangayCtrl',
		    resolve: {
	          getMunicipality: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'/getmunicipality/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})


	.when('/user', {
		templateUrl: 'views/accountlist.html',
		controller: 'registerCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/user/create', {
		templateUrl: 'views/register.html',
		controller: 'addAccountCtrl',
		resolve: {
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/user/:id', {
		templateUrl: 'views/register.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})


	.when('/myaccount/:id', {
		templateUrl: 'views/editaccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})


	.when('/regionadmin/:id', {
		templateUrl: 'views/regionAccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})


	.when('/useraccount/:id', {
		templateUrl: 'views/edituseraccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
	            }
	        }
	})


	.when('/clearance', {
		templateUrl: 'views/clearancelist.html',
		controller: 'clearanceCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/clearance/create', {
		templateUrl: 'views/clearance.html',
		controller: 'addclearanceCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/clearance/:id', {
		templateUrl: 'views/clearance.html',
		controller: 'editclearanceCtrl',
		    resolve: {
	          Clearance: function(Restangular, $route){
	            return Restangular.one('clearance', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})
	.when('/viewclearance/:id', {
		templateUrl: 'views/viewclearance.html',
		controller: 'editclearanceCtrl',
		    resolve: {
	          Clearance: function(Restangular, $route){
	            return Restangular.one('clearance', $route.current.params.id).get();
	          },
	           auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/resident', {
		templateUrl: 'views/residentlist.html',
		controller: 'residentCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/viewresident/:id', {
		templateUrl: 'views/viewresident.html',
		controller: 'viewresidentCtrl',
		 resolve: {
	          getbrgyresident: function(Restangular, $route){
	            return Restangular.one('resident', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/judicial', {
		templateUrl: 'views/judicialist.html',
		controller: 'judicialCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/judicial/create', {
		templateUrl: 'views/judicial.html',
		controller: 'addjudicialCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/judicial/:id', {
		templateUrl: 'views/judicial.html',
		controller: 'editjudicialCtrl',
		resolve: {
	          Judicial: function(Restangular, $route){
	            return Restangular.one('judicial', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})
	.when('/viewjudicial/:id', {
		templateUrl: 'views/viewjudicial.html',
		controller: 'editjudicialCtrl',
		resolve: {
	          Judicial: function(Restangular, $route){
	            return Restangular.one('judicial', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
             }
	        }
	})

            .when('/official-account', {
                templateUrl: 'views/officials/officialsaccountlist.html',
                controller: 'OfficialsaccountCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })

            .when('/viewreport/:id', {
                templateUrl: 'views/reports/viewreport.html',
                controller: 'reportCtrl',
                resolve: {
                      getreport: function(ReportService, $route){
                        return ReportService.GetReport($route.current.params.id);
                      },
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })
            .when('/generateIDs', {
                templateUrl: 'views/generateIDs/generateIds.html',
                controller: 'genrateIDsCtrl',
                 resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })

            .when('/businesspermit', {
                templateUrl : 'views/businesspermit/businesspermit.html',
                controller: 'businesspermitCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                }
            })

            .when('/officialsaccountlist', {
                templateUrl : 'views/officials/resident_officials.html',
                controller: 'OfficialsaccountCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                }
            })

	.otherwise({ redirectTo: '/login' });


	  RestangularProvider.setBaseUrl(DbCollection);
      RestangularProvider.setRestangularFields({
        id: '_id'
      });

      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {

        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      });

});

app.run(function($rootScope, $http, $location, loginCollection, DbCollection) {

    $rootScope.$on("$routeChangeSuccess", function (userInfo) {
    });

    $rootScope.$on("$routeChangeError", function (event, current, previous, eventObj) {
        if (eventObj.authenticated === false) {
        	$rootScope.errornotoken = 'Your access token has been deleted. Please login your account.';
            $location.url("/login");
        }
        // check if the UserAcount is exist, if not clear the sessionStorage
		   $http.get(DbCollection + 'account/')
		    .then(function(result){
		      $rootScope.UserAcount = result.data;
		      if($rootScope.UserAcount == null){
		        sessionStorage.clear();
		        $location.path('/login');
		        // window.location.reload();
		      }
		    });
    });
});

