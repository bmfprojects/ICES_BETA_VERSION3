var express                 = require('express'),
    config                      = require("./configs"),
    bodyparser              = require('body-parser'),
    mongodb                 = require('mongoskin'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    db                          = mongodb.db(config.mongodb.brgydata, {native_parser:true});
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid         = require('shortid');

//=========================================================================
//barangay business COLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/business')
    .get(function (req, res){
        db.collection('brgybusiness').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })
    .post(function (req, res) {
    var business          = req.body;
    var shortID                 = shortid.generate();
    business.shorid         = shortID;
    business.usercurrentId = req.user;
    var today = new Date();
                     var getExactDate  = function(getdate){

                            var days,
                                month;

                            var d = new Date(getdate);
                            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
       var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

                            var today = days[d.getUTCDay()];
                            var dd = days[d.getUTCDay()];
                            var mm = month[d.getMonth()]; //January is 0!
                            var yyyy = d.getFullYear();
                            var dy = d.getDate();

                            var todays = mm+' '+dy+', '+ yyyy;
                            return todays;

                    }

      business.dateCreated = getExactDate(today);

        db.collection('brgybusiness').insert(business, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/business/:id')
    .get(function (req, res){
        db.collection('brgybusiness').findById(req.params.id, function (err, data){
            return res.json(data);
        });
    })
    .put(function (req, res, next){
        var business = req.body;
        delete business._id;

            db.collection('brgybusiness').updateById(req.params.id, business, function (err, data){
                return res.json(data);
            });

    })
    .delete(function (req, res){
        db.collection('brgybusiness').removeById(req.params.id, function(){
            res.json(null);
        });
    });

module.exports = router;